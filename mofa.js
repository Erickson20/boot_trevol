const puppeteer = require('puppeteer-extra');
const moment = require("moment");
const Helpers = require("./helpers.js")


let wait = false


async function waitForText(page,text) {

   // await page.waitForXPath("//*[@class='"+className+"' and contains(., '"+text+"')]");
   await page.waitForFunction("document.querySelector('body').innerText.includes('"+text+"')");

    //   try {
    //     await page.waitForFunction(
    //       text => document.querySelector('body').innerText.includes(text),
    //       {},
    //       text
    //     );
    //     console.log('existe :>> ', text);
    //   } catch(e) {
    //     console.log(`The text "${text}" was not found on the page`);
    //   }
}

async function clickFirstButton(page,text,number,element) {
    //await page.waitFor(2000);
    page.waitForNavigation({waitUntil: 'networkidle2'})
    wait = true;
    console.log('intentando con:',text,wait);
    while (wait) {
        
        linkHandlers = await page.$x("//"+element+"[contains(text(), '"+text+"')]");
        //console.log('linkHandlers :>> ', linkHandlers);
        if(linkHandlers.length>0){
            wait = false
        }
    }

    //console.log('object :>> ', linkHandlers[number],number, text);
    await linkHandlers[number].click()
}


async function clickFirstButtonFile(page,text,number,element,filePath) {
    //await page.waitFor(2000);
    wait = true;
    console.log('intentando con:',text,wait);
    while (wait) {
        
        linkHandlers = await page.$x("//"+element+"[contains(text(), '"+text+"')]");
        //console.log('linkHandlers :>> ', linkHandlers);
        if(linkHandlers.length>0){
            wait = false
        }
    }

    //console.log('object :>> ', linkHandlers[number],number, text);
    console.log('linkHandlers :>> ', linkHandlers);
   // await linkHandlers[0].uploadFile(filePath);
}


async function select(page,value,time = 700) {
    
    await page.keyboard.press("Tab");
    await page.keyboard.type(value);
    await page.waitFor(time);
    await page.keyboard.press("Enter");
}



async function main(data) {
    //'C:/Program Files/Google/Chrome/Application/chrome.exe
    const result =   data.pasajeros;
    const browser = await puppeteer.launch({
        headless: false,
        args: ['--no-sandbox', '--disable-setuid-sandbox', ]
    });
    console.log('data :>> ', data);
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    await page.setViewport({ width: 1366, height: 768});
    await page.goto("https://evisa.mofa.gov.bs/bs-evics-entitlement-online-ui/", {waitUntil: 'networkidle0'});
   
    await waitForText(page,'Apply For Visa', 'v-captiontext')
    await clickFirstButton(page, 'Apply For Visa',0,'div')
    
    await waitForText(page,'Yes')
    await clickFirstButton(page, 'Yes',0,'label')

    await waitForText(page,'Visitor',)
    await clickFirstButton(page, 'Visitor',0,'label')


    await waitForText(page,'Self-Sponsored')
    await clickFirstButton(page, 'Self-Sponsored',0,'label')

    await page.waitFor(500);
    await waitForText(page,'Yes')
    await clickFirstButton(page, 'Yes',1,'label')

    await waitForText(page,'Start Application')
    await clickFirstButton(page, 'Start Application',0,'span')

    await waitForText(page,'Surname:')
    await waitForText(page,'Given name(s):')
    await waitForText(page,'Passport number:')
    await waitForText(page,'Date of birth (DD-MM-YYYY):')
    await waitForText(page,'Nationality:')
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.apellidos);

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.nombres);

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.fecha_nacimiento);

  
    await select(page,"DOMINICAN - DOM")
    
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.pasaporte);

    
    await waitForText(page,'Next')
    await clickFirstButton(page, 'Next',0,'span')

    await waitForText(page,'Document:')
    await waitForText(page,'Visa type:')
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");

    await select(page,"vacation")

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.flight_data.date_start);
    await page.keyboard.press("Tab");
    await page.keyboard.type( Helpers.getDaysByTwoDate(data.pasajeros.flight_data.date_start,data.pasajeros.flight_data.date_end).toString());

    await page.keyboard.press("Tab");
    await page.keyboard.type("2500");

    await select(page,"No")
    await select(page,"No")
    await select(page,"CONSULAR DIVISION HQ")
    await select(page,"ANY DAY")
    await select(page,"ANY TIME")
    
    await waitForText(page,'Next')
    await clickFirstButton(page, 'Next',0,'span')

   

   await waitForText(page,'Title:')
   await waitForText(page,'Maiden name:')
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab"); 

    data.pasajeros.personal_data.sexo = data.pasajeros.personal_data.sexo === 'F'? 'FEMALE':'MALE'
console.log('data.pasajeros.personal_data.sexo :>> ', data.pasajeros.personal_data.sexo);

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.sexo);
    await page.waitFor(300);
    await page.keyboard.press("ArrowDown");
    await page.keyboard.press("Enter");


    await select(page,"DOMINICAN REPUBLIC")


    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.lugar_nacimiento);
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.cedula.toString());

    await waitForText(page,'Next')
    await clickFirstButton(page, 'Next',0,'span')

   
    //await page.focus("//span[contains(text(), 'Passport number:')]")
    await waitForText(page,'Passport number:')
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.fecha_exp.toString());
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.fecha_vencimiento.toString());
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.lugar_nacimiento.toString());


    await select(page,"DOMINICAN REPUBLIC")

    await clickFirstButton(page, 'Next',0,'span')

    await waitForText(page,'Add Address')

    await clickFirstButton(page, 'Add Address',0,'span')
    await waitForText(page,'Address type:')

    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.address);
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.lugar_nacimiento);
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");

    await select(page,"DOMINICAN REPUBLIC")
    await waitForText(page,'Save')

    await clickFirstButton(page, 'Save',1,'span')

    await page.waitFor(500);
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.telefono.toString());
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.email.toString());
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.email.toString());
    await page.keyboard.press("Tab");


    await waitForText(page,'Save')

    await clickFirstButton(page, 'Next',0,'span')
    await waitForText(page,'Employment type:')
   

    
    await select(page,"EMPLOYED")
    await page.waitFor(500);
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.carta_trabajo_data.company);
    await page.waitFor(500);
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.carta_trabajo_data.tell_departament);
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.carta_trabajo_data.position);
    await page.keyboard.press("Tab");
    await page.keyboard.type(Helpers.calculateNoClient(2,8).toString());
    await page.keyboard.press("Tab");


    await waitForText(page,'Add Address')

    await clickFirstButton(page, 'Add Address',0,'span')

    
    await waitForText(page,'Address type:')

    await select(page,"WORK ADDRESS")

    await page.keyboard.press("Tab");
    await page.keyboard.type(Helpers.addressPersonalRandom(data.pasajeros.personal_data.lugar_nacimiento));
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");

    await page.keyboard.type(data.pasajeros.personal_data.lugar_nacimiento);
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");

    await select(page,"DOMINICAN REPUBLIC")

    await waitForText(page,'Save')
    await clickFirstButton(page, 'Save',1,'span')
    await page.waitFor(500);
    await waitForText(page,'Next')
    await clickFirstButton(page, 'Next',0,'span')

  
    await waitForText(page,'Marital status')
    await select(page,"SINGLE")

    await waitForText(page,'Add Parent')
    await clickFirstButton(page, 'Add Parent',0,'span')
    
    await waitForText(page,'Relationship:')
    await select(page,"FATHER")

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.parent.split(',')[1] + ' '+ data.pasajeros.personal_data.parent.split(',')[2]);
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.parent.split(',')[0]);

    await select(page,"DOMINICAN - DOM")
    await select(page,"CITIZEN")

    await waitForText(page,'Save')
    await clickFirstButton(page, 'Save',1,'span')
    await page.waitFor(500);
    await waitForText(page,'Add Family')
    await clickFirstButton(page, 'Add Family',0,'span')

    await waitForText(page,'Relationship:')
    await select(page,"SON")
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.children.split(',')[1] );
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.children.split(',')[1] );

    await select(page,"CITIZEN")

    await waitForText(page,'Save')
    await clickFirstButton(page, 'Save',1,'span')
    await page.waitFor(500);
    await waitForText(page,'Add Contact')
    await clickFirstButton(page, 'Add Contact',0,'span')

    await waitForText(page,'Relationship:')
    await select(page,"COUSIN")

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.name_contact_emer );

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.name_contact_emer );

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.tel_contact_emer.toString() );

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.address_contact_emer);

    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.personal_data.lugar_nacimiento );

    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");

    await select(page,"DOMINICAN REPUBLIC")

    await waitForText(page,'Save')
    await clickFirstButton(page, 'Save',1,'span')
    await page.waitFor(500);
    await waitForText(page,'Next')
    await clickFirstButton(page, 'Next',0,'span')
    
    await waitForText(page,'Name of hotel:')
    await page.keyboard.press("Tab");
    await page.keyboard.type(data.pasajeros.hotel_data.hotel_name );

    await page.keyboard.press("Tab");
    await page.keyboard.type(Helpers.personaRandomName() );
    
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");
    await page.keyboard.type(Helpers.calculateNoClient(34321976544,34328976544).toString());
    await select(page,"AIRCRAFT")
    await page.keyboard.press("Tab");

    await waitForText(page,'Add Address')
    await clickFirstButton(page, 'Add Address',0,'span')
    
    await waitForText(page,'Address type:')
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");


    await page.keyboard.type(data.pasajeros.hotel_data.hotel_address );
    
    await page.keyboard.press("Tab");
    await page.keyboard.press("Tab");

    await page.keyboard.type(data.pasajeros.flight_data.destino) 
    
    await select(page,"GRAND BAHAMA")


    await waitForText(page,'Save')
    await clickFirstButton(page, 'Save',1,'span')
    await page.waitFor(500);
    await waitForText(page,'Next')
    await clickFirstButton(page, 'Next',0,'span')

    await waitForText(page,'Applicant has a criminal conviction:')


    await select(page,"NO")
    await select(page,"NO")
    await select(page,"NO")
    await select(page,"NO")
    await select(page,"NO")

    await waitForText(page,'Next')
    await clickFirstButton(page, 'Next',0,'span')

    await waitForText(page,'Photo') 
    await clickFirstButton(page, 'Select Photo',0,'span')
    // await clickFirstButtonFile(
    //     page, 
    //     'Select Photo',
    //     0,
    //     'span',
    //     __dirname + 'pasajeros/'+ 
    //     data.pasajeros.personal_data.nombres + 
    //     data.pasajeros.personal_data.apellidos
    // )
try {
    const elementHandle = await page.$("input[type=file]");
    console.log('elementHandle :>> ', elementHandle);
    await elementHandle.uploadFile( __dirname + '/pasajeros/'+ 
    data.pasajeros.personal_data.nombres + ' '+ 
    data.pasajeros.personal_data.apellidos + '/foto.jpeg');
    await page.keyboard.press("Escape")
} catch (error) {
    console.log('error Foto no existe :>> ', error);
}
    
    








    



}





module.exports = {
    main,
}
