const https = require("https");
const path = require("path");
const fs = require("fs");
const request = require("request");

const API_KEY = "ericksonflores20@gmail.com_10aef65797bbe9291e3e6c4945ca253331be172cd3e16f96963e14527ef81455b8e57afd"

// // Source PDF file
// const SourceFile = "/Users/ericksonflores/Documents/proyectos/boot_trevol/pasajeros/JUAN BAUTISTA PEGUERO ABREU/JUAN BAUTISTA PEGUERO ABREU - hotel.docx";
// // PDF document password. Leave empty for unprotected documents.
// const Password = "";
// // Destination PDF file name
// const DestinationFile = "/Users/ericksonflores/Documents/proyectos/boot_trevol/pasajeros/JUAN BAUTISTA PEGUERO ABREU/hotel.pdf";





async function convetToPdf(SourceFile,DestinationFile) {
    // 1. RETRIEVE PRESIGNED URL TO UPLOAD FILE.
    await getPresignedUrl(API_KEY, SourceFile)
    .then(([uploadUrl, uploadedFileUrl]) => {
        // 2. UPLOAD THE FILE TO CLOUD.
        uploadFile(API_KEY, SourceFile, uploadUrl)
            .then(() => {
                // 3. CONVERT UPLOADED DOC (DOCX) FILE TO PDF
                convertDocToPdf(API_KEY, uploadedFileUrl, DestinationFile);
            })
            .catch(e => {
                console.log(e);
            });
    })
    .catch(e => {
        console.log(e);
    });
    
}


function getPresignedUrl(apiKey, SourceFile) {
    return new Promise(resolve => {
        // Prepare request to `Get Presigned URL` API endpoint
        let queryPath = `/v1/file/upload/get-presigned-url?contenttype=application/octet-stream&name=${path.basename(SourceFile)}`;
        let reqOptions = {
            host: "api.pdf.co",
            path: encodeURI(queryPath),
            headers: { "x-api-key": API_KEY }
        };
        // Send request
        https.get(reqOptions, (response) => {
            response.on("data",  (d)  => {
                console.log("Sdfsdfdsfsdf");
                let data =  JSON.parse(d);
                if (data.error == false) {
                    // Return presigned url we received
                    resolve([data.presignedUrl, data.url]);
                }
                else {
                    // Service reported error
                    console.log("getPresignedUrl(): " + data.message);
                }
            });
        })
            .on("error", (e) => {
                // Request error
                console.log("getPresignedUrl(): " + e);
            });
    });
}

function uploadFile(apiKey, SourceFile, uploadUrl) {
    return new Promise(resolve => {
        fs.readFile(SourceFile, (err, data) => {
            request({
                method: "PUT",
                url: uploadUrl,
                body: data,
                headers: {
                    "Content-Type": "application/octet-stream"
                }
            }, (err, res, body) => {
                if (!err) {
                    resolve();
                }
                else {
                    console.log("uploadFile() request error: " + e);
                }
            });
        });
    });
}

function convertDocToPdf(apiKey, uploadedFileUrl, destinationFile) {
    // Prepare URL for `DOC To PDF` API call
    let queryPath = `/v1/pdf/convert/from/doc`;

    // JSON payload for api request
    var jsonPayload = JSON.stringify({
        name: path.basename(destinationFile), url: uploadedFileUrl
    });

    var reqOptions = {
        host: "api.pdf.co",
        method: "POST",
        path: queryPath,
        headers: {
            "x-api-key": apiKey,
            "Content-Type": "application/json",
            "Content-Length": Buffer.byteLength(jsonPayload, 'utf8')
        }
    };

    // Send request
    var postRequest = https.request(reqOptions, (response) => {
        response.on("data", (d) => {
            response.setEncoding("utf8");
            // Parse JSON response
            let data = JSON.parse(d);
            if (data.error == false) {
                // Download PDF file
                var file = fs.createWriteStream(destinationFile);
                https.get(data.url, (response2) => {
                    response2.pipe(file)
                        .on("close", () => {
                            console.log(`Generated PDF file saved as "${destinationFile}" file.`);
                        });
                });
            }
            else {
                // Service reported error
                console.log("readBarcodes(): " + data.message);
            }
        });
    })
        .on("error", (e) => {
            // Request error
            console.log("readBarcodes(): " + e);
        });

    // Write request data
    postRequest.write(jsonPayload);
    postRequest.end();
}


function test() {


  

     fetch("https://arajet-api.ezycommerce.sabre.com/api/v1/Booking/Save", {
    "headers": {
        "accept": "text/plain",
        "accept-language": "es-MX,es;q=0.9",
        "access-control-allow-origin": "*",
        "appcontext": "manage",
        "content-type": "application/json;charset=UTF-8",
        "languagecode": "es-do",
        "sec-ch-ua": "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Google Chrome\";v=\"108\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"macOS\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site",
        "sessiontoken": "IrRrzeoAEDFB6BQYBSXSXJLJn6KMrBo/ZipU24D79hBognsF60NT9AyWu+m0+1tdKHuNng6t17V/lThndJdvODbsnMRm9hNSER27sdyG1P7smBLSqvGQ+T0ypEc04DIELRcS+SvIZiJ2W4ukuRr6319I45tKBt5uPMemqD/ulYCjrEkAkfJnd2hlzmNR9Vd5xmGrg23P+SStfVJw001I8C0DAQx3HBAtii+K+9FC9PR5bVwSIjitiKdWMCNOfaQA2nd2q7y2dtg/OnRIsXOLtA==",
        "tenant-identifier": "caTRCudVPKmnHNnNeTgD3jDHwtsVvNtTmVHnRhYQzEqVboamwZp6fDEextRR8DAB",
        "x-useridentifier": "XTwJUjCAVUhAuIt60wMiSySts8arK8"
    },
    "referrer": "https://www.arajet.com/",
    "referrerPolicy": "strict-origin-when-cross-origin",
    "body": "{\"confirmationNumber\":\"01RLMF\",\"bookingLastName\":\"NOVA\",\"isModified\":true,\"isSaveAfterCancel\":false,\"receiptLanguageCode\":\"es-do\",\"areSeatsCached\":false,\"currency\":\"USD\",\"languageCode\":\"es-do\"}",
    "method": "POST",
    "mode": "cors",
    "credentials": "omit"
    });
}




function test2() {
  
    fetch("https://arajet-api.ezycommerce.sabre.com/api/v1/Booking/Cancel", {
        "headers": {
          "accept": "text/plain",
          "accept-language": "es-MX,es;q=0.9",
          "access-control-allow-origin": "*",
          "appcontext": "manage",
          "content-type": "application/json;charset=UTF-8",
          "languagecode": "es-do",
          "sec-ch-ua": "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Google Chrome\";v=\"108\"",
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": "\"macOS\"",
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          "sessiontoken": "ACSs161/NGRbLGCnMf5RS+QSdrb7VmvQfTSoZFkilKG4p2ZiaNaDlDDsRSpB+67ZBkWYZTSr5iRlUu3QndwgvxUFuBecfnXC5p0UWNjbJwN5xkefu2nE7WPWKBpnbbAgQE8lkP3tQR5FRMHWW5FO4k2Z9PLIRd4exMxVaM+TLgbHP8imKW62xX595EmxB1iL6q5nSb6wrrCzFBXzwz+MMT96Nj+j90WOtsY9Ng+HLG1prdkIaKzkcs5g+DjliWFEuIit20W14ZNjKjZH77/iZQ==",
          "tenant-identifier": "caTRCudVPKmnHNnNeTgD3jDHwtsVvNtTmVHnRhYQzEqVboamwZp6fDEextRR8DAB",
          "x-useridentifier": "XTwJUjCAVUhAuIt60wMiSySts8arK8"
        },
        "referrer": "https://www.arajet.com/",
        "referrerPolicy": "strict-origin-when-cross-origin",
        "body": "{\"confirmationNumber\":\"Y5CQVJ\",\"bookingLastName\":\"NOVA\",\"isModified\":false,\"historicUsername\":null,\"receiptLanguageCode\":\"es-do\",\"languageCode\":\"es-do\"}",
        "method": "POST",
        "mode": "cors",
        "credentials": "omit"
      });


      fetch("https://arajet-api.ezycommerce.sabre.com/api/v1/Booking/Save", {
        "headers": {
            "accept": "text/plain",
            "accept-language": "es-MX,es;q=0.9",
            "access-control-allow-origin": "*",
            "appcontext": "manage",
            "content-type": "application/json;charset=UTF-8",
            "languagecode": "es-do",
            "sec-ch-ua": "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Google Chrome\";v=\"108\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"macOS\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site",
            "sessiontoken": "1ZMaBtYJGYnKwFH4YXfF4invLhSS75f61VIyrUOuuDG0QF/k9S4qu+QXlzVzS35THBfEsDshtqSZdi1DhvmgtJ1yfdSsIlJnn07/IgqBqzffk0DCnzMw6bL5NbdUqy9idMkwaq0D0kJVO0+4AgmHGXHjUUiT8xQ72AMeol2bD6Ygr8n/bXAG5D/4E+aCowIXRhxXCuEcQJ/5XsZkS3JM/lr/ddL7DkSrQJrUEQxj+i1BJ11aLzU0P92oTK/oId/zY9EYIAZnKa2kVexlmNkpZA==",
            "tenant-identifier": "caTRCudVPKmnHNnNeTgD3jDHwtsVvNtTmVHnRhYQzEqVboamwZp6fDEextRR8DAB",
            "x-useridentifier": "2FHIW62zUGR36ilOa68jUh80iRBDzv"
        },
        "referrer": "https://www.arajet.com/",
        "referrerPolicy": "strict-origin-when-cross-origin",
        "body": "{\"confirmationNumber\":\"Y5CQVJ\",\"bookingLastName\":\"NOVA\",\"isModified\":true,\"isSaveAfterCancel\":true,\"receiptLanguageCode\":\"es-do\",\"areSeatsCached\":false,\"currency\":\"USD\",\"languageCode\":\"es-do\"}",
        "method": "POST",
        "mode": "cors",
        "credentials": "omit"
      });


}



    module.exports = {
        convetToPdf
    }

