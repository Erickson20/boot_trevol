const moment =  require("moment");

const schemaDocuments = {
    'nombres': {
        prop: 'nombre',
        type: String
    },
    'apellidos': {
        prop: 'apellidos',
        type: String
    },
    'cedula': {
        prop: 'cedula',
        type: String
    },
    'pasaporte': {
        prop: 'pasaporte',
        type: String
    },
    'nacimiento': {
        prop: 'nacimiento',
        type: String
    },
    'sexo': {
        prop: 'sexo',
        type: String
    },
    'lugar_nacimiento': {
        prop: 'lugar_nacimiento',
        type: String
    },
    'destino': {
        prop: 'destino',
        type: String
    },
    'entrada': {
        prop: 'entrada',
        type: String
    },
    'carta_banco': {
        prop: 'carta_banco',
        type: String
    },
    'hotel': {
        prop: 'hotel',
        type: String
    },
    'vuelo': {
        prop: 'vuelo',
        type: String
    },
    'carta_trabajo': {
        prop: 'carta_trabajo',
        type: String
    },
    'certificado_medico': {
        prop: 'certificado_medico',
        type: String
    },
    'buena_conducta': {
        prop: 'buena_conducta',
        type: String
    },


   

}


module.exports = schemaDocuments;
