const Docx =  require("./docx.js")
const data = require("./data.js")
const fs = require('fs');
const Ticket = require("./eTicket.js")
const BookingCancel = require("./cancelBooking.js")
const schema = require('./scheme.js')
const schemaCancel = require('./scheme_cancel.js')
const Helpers = require('./helpers.js')
const Pase = require('./index.js')
const xlsxFile = require('read-excel-file/node');
const Flight = require("./flight_ida");

console.log("****************************** HOLA, SOY TREVOL*******************************")
console.log("Estoy buscando los pasajeros.......")


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


//flight_cancel
xlsxFile('template/pasajeros.xlsx',{ schema }).then(async (rows) => {

    const result = rows.rows
    let c = 0;
    let return_f = 0;
    for (const i in result) {
        c++
        return_f++
        let psg = data(result[i])
        
        if(result[i].pcr){
            console.log('Estoy creando su PCR....')
            Docx.pcr(psg.pasajeros,'template/'+result[i].pcr+'.docx');
        }

        if(result[i].hotel){    
            console.log('Estoy creando su HOTEL....')
            Docx.hotel(psg.pasajeros,'template/hotel'+result[i].hotel+'_'+c+'.docx');
            Docx.recibo(psg.pasajeros,'template/recibo'+result[i].hotel+'.docx')
        }



        if(result[i].pase){
            console.log('Estoy creando su pase de salud....')
            await Pase.main(psg)
        }

        if(result[i].seguro){
            console.log('Estoy creando su seguro....')
            Docx.seguro(psg.pasajeros,'template/seguro'+result[i].seguro+'_'+c+'.docx');
        }
        Helpers.createTourTemplate(psg,c)
        Docx.flightReturn(psg.pasajeros,'template/regreso_'+return_f+'_'+c+'.docx')

        // if(result[i].ticket){
        //     console.log('Estoy creando su Ticket....')
        //     await Ticket.main(psg)
        // }
        if( c==3){
            c = 0;
        }

        if( return_f==2){
            return_f = 0;
        }


    }


    for (const i in result) {
        let psg = data(result[i])

        console.log('object :>> ', result[i].ticket);
        switch (result[i].ticket) {
            case 1:
                console.log('Estoy creando su Ticket de salidad con la version 1....')
                let cont = true;
                await Ticket.main(psg)
                
                console.log('TENEMOS EL ETICKET DE SALIDA DE ', psg.pasajeros.nombre);
                // while (cont) {
                //     try {
                //         await Ticket.main(psg)
                //         console.log('TENEMOS EL ETICKET DE ', psg.pasajeros.nombre);
                //         cont =  false
                //     } catch (error) {
                //         console.log('ESTAMOS INTENTADO OTRA VEZ CON ', psg.pasajeros.nombre);
                //         cont = true
                //     }
                // }
                break;
            case 2:
                console.log('Estoy creando su Ticket de entrada con la version 1....')
                let cont2 = true;
                await Ticket.mainBack(psg)
                console.log('TENEMOS EL ETICKET DE ENTRADA DE ', psg.pasajeros.nombre);
                break;

            case 3:
                console.log('Estoy creando su Ticket con la version 2....')
                Docx.eticket(psg.pasajeros,'template/eticket.docx')
                break;
            default:
                console.log('NO HAY OPCION DE ETICKET');
                break;
        }
       

    }
    console.log('***************BIEN HE TERMINADO, PUEDES CERRAR MI VENTANA!***************')

}).catch((e)=>{
    console.log(e)
})






// async function start(params) {
//     await Flight.main();
// }
// start();
