const puppeteer = require('puppeteer-extra');
const RecaptchaPlugin = require('puppeteer-extra-plugin-recaptcha');

puppeteer.use(
  RecaptchaPlugin({
    provider: { id: '2captcha', token: '125d5d013ba9764502a095654a520f9d' }
  })
);

(async () => {
  const browser = await puppeteer.launch({ headless: false });

  let page = await browser.newPage();

  await page.goto('https://eticket.migracion.gob.do/Auth/TravelRegister/');

  const { solved, error } = await page.solveRecaptchas();
  if(solved) {
    console.log('✔️ The captcha has been solved');
  }
})();