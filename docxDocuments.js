const PizZip = require('pizzip');
const Docxtemplater = require('docxtemplater');
const fs = require('fs');
const fs2 = require('fs').promises;
const path = require('path') ;
const Helpers = require("./helpers.js");
const libre = require('libreoffice-convert');
libre.convertAsync = require('util').promisify(libre.convert);

// The error object contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
function replaceErrors(key, value) {
    if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function(error, key) {
            error[key] = value[key];
            return error;
        }, {});
    }
    return value;
}

function errorHandler(error) {
    console.log(JSON.stringify({error: error}, replaceErrors));

    if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors.map(function (error) {
            return error.properties.explanation;
        }).join("\n");
        console.log('errorMessages', errorMessages);
        // errorMessages is a humanly readable message looking like this :
        // 'The tag beginning with "foobar" is unopened'
    }
    throw error;
}

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

async function createPdf(dirIn, dirOut, file) {
    if (!fs.existsSync(dirOut+'/PDF')) {
        fs.mkdirSync(dirOut+'/PDF');
    }
    let name = file.split('.')[0]
    const inputPath = path.join(dirIn, file);
    const outputPath = path.join(dirOut, 'PDF/'+name+'.pdf');
    try {
        const docxBuf = await fs2.readFile(inputPath);
        let pdfBuf = await libre.convertAsync(docxBuf, '.pdf', undefined);
        await fs2.writeFile(outputPath, pdfBuf);
        console.log('proceso pdf terminado ',);
    } catch (error) {
        console.log('espere un momento... convirtiendo', error.errno);
        if(error.errno !== -2){
            console.log('error :>> ', error);
        }
    }
} 

function familyInfo(result) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
   // fs.writeFileSync(dir, JSON.stringify(result.pasajeros.personal_data));
    fs.writeFileSync(dir+'/'+result.pasajeros.personal_data.nombres+'-MOFA.json', JSON.stringify(result));
   
}
async function banco(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    
    if (!fs.existsSync(dir)) {
        fs.mkdirSync( __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos);
        //fs.mkdirSync(dir);
    }

    if (!fs.existsSync(dir+'/WORD')) {
        fs.mkdirSync( __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos+'/WORD');
        //fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        cuenta:  result.pasajeros.banco_data.cuenta  ,
        apertura:  result.pasajeros.banco_data.fecha_apertura  ,
        deposito:  result.pasajeros.banco_data.fecha_deposito  ,
        balance_actual:  result.pasajeros.banco_data.balance_actual  ,
        balance_disponible:  result.pasajeros.banco_data.balance_disponible,
        promedio:  result.pasajeros.banco_data.promedio,
        destinatario:  result.pasajeros.banco_data.destinatario,
        fecha_doc:  result.pasajeros.banco_data.fecha_doc,

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
fs.writeFileSync(path.resolve(dir+'/WORD', 'BANCO ESP.docx'), buf);
await createPdf(dir+'/WORD',dir,'BANCO ESP.docx')


}

async function banco_english(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        cuenta:  result.pasajeros.banco_data.cuenta  ,
        apertura:  result.pasajeros.banco_data.fecha_apertura  ,
        deposito:  result.pasajeros.banco_data.fecha_deposito  ,
        balance_actual:  result.pasajeros.banco_data.balance_actual  ,
        balance_disponible:  result.pasajeros.banco_data.balance_disponible,
        promedio:  result.pasajeros.banco_data.promedio,
        destinatario:  result.pasajeros.banco_data.destinatario,
        fecha_doc:  result.pasajeros.banco_data.fecha_doc,
        banco:  result.pasajeros.banco_data.banco,
        gerente: result.pasajeros.banco_data.gerente,
        dateRequestEng:result.pasajeros.banco_data.dateRequestEng

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', 'BANCO ENG.docx'), buf);
    await createPdf(dir+'/WORD',dir,'BANCO ENG.docx')
   // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}

async function trabajo(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        position:  result.pasajeros.carta_trabajo_data.position  ,
        date_start:  result.pasajeros.carta_trabajo_data.date_start  ,
        depertament:  result.pasajeros.carta_trabajo_data.depertament  ,
        salario:  result.pasajeros.carta_trabajo_data.salario  ,
        date_request:  result.pasajeros.carta_trabajo_data.date_request  ,
        gerente:  result.pasajeros.carta_trabajo_data.gerente  ,
        destinatario:  result.pasajeros.carta_trabajo_data.destinatario  ,
        tell_departament:  result.pasajeros.carta_trabajo_data.tell_departament  ,
        dateRequestEng:  result.pasajeros.carta_trabajo_data.dateRequestEng  ,
        date_request:  result.pasajeros.carta_trabajo_data.date_request  ,
        dayRequest: result.pasajeros.carta_trabajo_data.dateRequestSpa.dia,
        dayRequestNumber: result.pasajeros.carta_trabajo_data.dateRequestSpa.diaNumber,
        mes_request: result.pasajeros.carta_trabajo_data.dateRequestSpa.mes,

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', 'TRABAJO ESP.docx'), buf);
    await createPdf(dir+'/WORD',dir,'TRABAJO ESP.docx')
   // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}

async function trabajo_english(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        position:  result.pasajeros.carta_trabajo_data.position  ,
        company:  result.pasajeros.carta_trabajo_data.company  ,
        date_start:  result.pasajeros.carta_trabajo_data.date_start  ,
        depertament:  result.pasajeros.carta_trabajo_data.depertament  ,
        salario:  result.pasajeros.carta_trabajo_data.salario  ,
        date_request:  result.pasajeros.carta_trabajo_data.date_request  ,
        gerente:  result.pasajeros.carta_trabajo_data.gerente  ,
        destinatario:  result.pasajeros.carta_trabajo_data.destinatario  ,
        tell_departament:  result.pasajeros.carta_trabajo_data.tell_departament  ,
        dateRequestEng:  result.pasajeros.carta_trabajo_data.dateRequestEng  ,
        date_request:  result.pasajeros.carta_trabajo_data.date_request  ,
        dayRequest: result.pasajeros.carta_trabajo_data.dateRequestSpa.dia,
        dayRequestNumber: result.pasajeros.carta_trabajo_data.dateRequestSpa.diaNumber,
        mes_request: result.pasajeros.carta_trabajo_data.dateRequestSpa.mes,

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', 'TRABAJO ENG.docx'), buf);
    await createPdf(dir+'/WORD',dir,'TRABAJO ENG.docx')
   // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}

async function medico(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        doctor:  result.pasajeros.certificado_medico_data.doctor,
        exequartur:  result.pasajeros.certificado_medico_data.exequartur,
        edad_paciente:  result.pasajeros.certificado_medico_data.edad_paciente,
        date_request:  result.pasajeros.certificado_medico_data.date_request,
        address_hospital:  result.pasajeros.certificado_medico_data.address_hospital,

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', 'MEDICO ESP.docx'), buf);
    await createPdf(dir+'/WORD',dir,'MEDICO ESP.docx')
   // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}


async function medico_english(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        doctor:  result.pasajeros.certificado_medico_data.doctor,
        exequartur:  result.pasajeros.certificado_medico_data.exequartur,
        edad_paciente:  result.pasajeros.certificado_medico_data.edad_paciente,
        date_request:  result.pasajeros.certificado_medico_data.date_request,
        address_hospital:  result.pasajeros.certificado_medico_data.address_hospital,
        dateRequestEng:result.pasajeros.certificado_medico_data.dateRequestEng,

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', 'MEDICO ENG.docx'), buf);
    await createPdf(dir+'/WORD',dir,'MEDICO ENG.docx')
   // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}

async function conducta(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        date_request:  result.pasajeros.buena_conducta_data.date_request,
        cis:  result.pasajeros.buena_conducta_data.cis,
        cas:  result.pasajeros.buena_conducta_data.cas,
        dateRequestForWritten:  result.pasajeros.buena_conducta_data.dateRequestForWritten,
        mes_request: result.pasajeros.buena_conducta_data.dateRequestForWritten.mes,
        dayNumber: result.pasajeros.buena_conducta_data.dateRequestForWritten.diaNumber,
        day: result.pasajeros.buena_conducta_data.dateRequestForWritten.dia

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', 'BUENACONDUCTA ESP.docx'), buf);
    await createPdf(dir+'/WORD',dir,'BUENACONDUCTA ESP.docx')
   // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}

async function conducta_english(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        date_request:  result.pasajeros.buena_conducta_data.date_request,
        cis:  result.pasajeros.buena_conducta_data.cis,
        cas:  result.pasajeros.buena_conducta_data.cas,
        dateRequestForWritten:  result.pasajeros.buena_conducta_data.dateRequestForWritten,
        mes_request: result.pasajeros.buena_conducta_data.dateRequestForWritten.mes,
        dayNumber: result.pasajeros.buena_conducta_data.dateRequestForWritten.diaNumber,
        day: result.pasajeros.buena_conducta_data.dateRequestForWritten.dia,
        dateRequestEng:result.pasajeros.buena_conducta_data.dateRequestEng

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', 'BUENACODUCTA ENG.docx'), buf);
    await createPdf(dir+'/WORD',dir,'BUENACODUCTA ENG.docx')
   // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}
async function vuelo(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        nombre: result.pasajeros.personal_data.nombres,
        apellidos: result.pasajeros.personal_data.apellidos,
        cedula: result.pasajeros.personal_data.cedula ,
        destino:  result.pasajeros.flight_data.destino === 'BAHAMAS'? 'Nassau(NAS)': 'Tortola(EIS)',
        date_start:  result.pasajeros.flight_data.date_start,
        date_end:  result.pasajeros.flight_data.date_end,
        booking:  result.pasajeros.flight_data.booking,
        id:Helpers.calculateNoClient(72149879301030,72909879301030).toString()

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', result.pasajeros.personal_data.nombres+ ' ' +result.pasajeros.personal_data.apellidos+' - vuelo.docx'), buf);
    await createPdf(dir+'/WORD',dir,result.pasajeros.personal_data.nombres+ ' ' +result.pasajeros.personal_data.apellidos+' - vuelo.docx')
    // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}

async function hotel(result,template) {
    let dir = __dirname + '/pasajeros/'+result.pasajeros.personal_data.nombres+' '+result.pasajeros.personal_data.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
  

    doc.setData({
        first_name: result.pasajeros.personal_data.nombres,
        last_name: result.pasajeros.personal_data.apellidos,
        booking_id: Helpers.calculateNoClient(156111302,177232413) ,
        booking_ref: Helpers.calculateNoClient(3344738191,3455849294)  ,
        member_id: Helpers.calculateNoClient(222904,293456),
        hotel_name: result.pasajeros.hotel_data.hotel_name,
        hotel_address: result.pasajeros.hotel_data.hotel_address,
        hotel_country:result.pasajeros.hotel_data.hotel_country,
        date_start: result.pasajeros.hotel_data.date_start,
        date_end: result.pasajeros.hotel_data.date_end

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir+'/WORD', result.pasajeros.personal_data.nombres+ ' ' +result.pasajeros.personal_data.apellidos+' - hotel.docx'), buf);
    
    await createPdf(dir+'/WORD',dir,result.pasajeros.personal_data.nombres+ ' ' +result.pasajeros.personal_data.apellidos+' - hotel.docx')
    //Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - hotel',dir)
}














module.exports = {
    banco,
    trabajo,
    trabajo_english,
    medico,
    medico_english,
    conducta,
    conducta_english,
    hotel,
    vuelo,
    banco_english,
    familyInfo
}

