const puppeteer = require('puppeteer-extra');
const fetch = require('node-fetch');




async function main(data) {
    //'C:/Program Files/Google/Chrome/Application/chrome.exe
    console.log('Pasajero :>> ', data);
    const browser = await puppeteer.launch({
        headless: false,
        args: ['--no-sandbox', '--disable-setuid-sandbox', ]
    });

    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    await page.setViewport({ width: 1366, height: 768});
    await  page.goto("https://www.arajet.com/es/manage?confirmationNumber="+data.no_booking+"&bookingLastName="+data.lastname, {waitUntil: 'networkidle0'});
  
   
    await page.waitForSelector('#modify-flights > div.mmb-section.selected-flights > div > div.mmb-change-buttons.hide-print > div')
    await page.evaluate(async()=>{
        let b = document.querySelector('#modify-flights > div.mmb-section.selected-flights > div > div.mmb-change-buttons.hide-print > div').children[0]
        b.click();
        console.log('document.v :>> ',b );
        return 
    })
    console.log('Estamos validando el token para cancelar el vuelo...');
    await page.waitForSelector('#modify-flights > div.mmb-section.selected-flights > div.vm--container > div.vm--modal > div > div:nth-child(2) > div.modal-body > div > div:nth-child(2) > div')

    await page.evaluate(async()=>{
        let b2 = document.querySelector('#modify-flights > div.mmb-section.selected-flights > div.vm--container > div.vm--modal > div > div:nth-child(2) > div.modal-body > div > div:nth-child(2) > div').children[0]
        b2.click();
        console.log('document.v :>> ',b2 );
        return 
    })
    let token = {}
     

     token = await new Promise((resolve, reject) => {
        page.on('request', async (req,headers)  => {
            if(req.headers().sessiontoken){
                resolve(req.headers())
            }
         })
     })
     console.log('Token valido, espere....');

      page.on('console',await sendRequest('Booking/Cancel',data,token))
      console.log('Cancelando el vuelo....');

      page.on('console',await sendRequest('Booking/Save',data,token))
      console.log('El vuelo de '+data.lastname+' esta cancelado revisa el correo!!');
      console.log('***********************************************************************');

}














  async function sendRequest(path,psg,token) {
    let url = 'https://arajet-api.ezycommerce.sabre.com/api/v1';
    await  fetch(url+"/"+path, {
        "headers": {
            "accept": "application/json",
            "Content-type":"application/json",
            "accept-language": "en-US,en;q=0.9,es;q=0.8",
            "access-control-allow-origin": "*",
            "appcontext": "manage",
            "languagecode": "es-do",
            "sec-ch-ua": "\"Google Chrome\";v=\"111\", \"Not(A:Brand\";v=\"8\", \"Chromium\";v=\"111\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"macOS\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site",
            "sessiontoken": token.sessiontoken,
            "tenant-identifier": "caTRCudVPKmnHNnNeTgD3jDHwtsVvNtTmVHnRhYQzEqVboamwZp6fDEextRR8DAB",
            "x-clientversion": "0.4.1441",
            "x-useridentifier": "2FHIW62zUGR36ilOa68jUh80iRBDzv"
          },
            
        "referrer": "https://www.arajet.com/",
        "referrerPolicy": "strict-origin-when-cross-origin",
        "body": "{\"confirmationNumber\":\""+psg.no_booking+"\",\"bookingLastName\":\""+psg.lastname+"\",\"isModified\":true,\"isSaveAfterCancel\":true,\"receiptLanguageCode\":\"es-do\",\"areSeatsCached\":false,\"currency\":\"USD\",\"languageCode\":\"es-do\"}",
        "method": "POST",
        "mode": "cors",
        "credentials": "omit"
        })
        .then(x => x.json())
        .then((x) => console.log(path+' :>> ', x))
  }




  async function valdiateToken(token) {
    await fetch("https://arajet-api.ezycommerce.sabre.com/api/v1/Security/ValidateSecurityToken", {
      "headers": {
        "accept": "text/plain",
        "accept-language": "en-US,en;q=0.9,es;q=0.8",
        "access-control-allow-origin": "*",
        "appcontext": "manage",
        "cache-control": "no-cache",
        "languagecode": "es-do",
        "pragma": "no-cache",
        "sec-ch-ua": "\"Not/A)Brand\";v=\"99\", \"Google Chrome\";v=\"115\", \"Chromium\";v=\"115\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"macOS\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site",
        "sessiontoken": token,
        "tenant-identifier": "caTRCudVPKmnHNnNeTgD3jDHwtsVvNtTmVHnRhYQzEqVboamwZp6fDEextRR8DAB",
        "x-clientversion": "0.4.1873",
        "x-useridentifier": "2FHIW62zUGR36ilOa68jUh80iRBDzv"
      },
      "referrer": "https://www.arajet.com/",
      "referrerPolicy": "strict-origin-when-cross-origin",
      "body": null,
      "method": "GET",
      "mode": "cors",
      "credentials": "omit"
    })
    .then(x => x.json())
    .then((x) => console.log(' :>> ', x))
    
  }





module.exports = {
    main
}




