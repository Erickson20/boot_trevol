const Helpers = require("./helpers.js")
const moment =  require("moment");



function dataDocuments(passager){
        let addressPersona = Helpers.addressPersonalRandom(passager[7])
        let families = Helpers.familyName(passager[6],passager[2])      
        let dateRequestEng = Helpers.getDateWrittenEng(Helpers.removeAndAddDayTodayForWritten(Helpers.calculateNoClient(-1,-3)))
        let dateRequesBuenaConducta = Helpers.removeAndAddDayTodayForWritten(Helpers.calculateNoClient(-4,-10))
        let dateRequestSpa = Helpers.getDateWrittenSpa(dateRequesBuenaConducta)
    
    return {
        "pasajeros":
            {
                personal_data:{
                    "nombres": passager[1],
                    "apellidos": passager[2],
                    "cedula": passager[3],
                    "pasaporte": passager[4],
                    "fecha_nacimiento": moment(moment(passager[5],'DD-MM-YYYY')).format('DD-MM-YYYY'),
                    "sexo": passager[6],
                    "lugar_nacimiento": passager[7],
                    "telefono":Helpers.calculateNoClient(8092224311,8099224311).toString(),
                    "children": families.children,
                    "couple":families.couple,
                    "address":addressPersona,
                    "parent":Helpers.parentRandomName(passager[2]),
                    "email":passager[18],
                    "fecha_exp": moment(moment(passager[16],'DD-MM-YYYY')).format('DD-MM-YYYY'),
                    "fecha_vencimiento": moment(moment(passager[17],'DD-MM-YYYY')).format('DD-MM-YYYY'),
                    "name_contact_emer":families.contact_emer,
                    "tel_contact_emer":Helpers.calculateNoClient(8092224311,8099224311).toString(),
                    "address_contact_emer":Helpers.addressPersonalRandom(passager[7])

                },
                flight_data:{
                    "destino": passager[8],
                    "date_start": moment(moment(passager[9],'DD-MM-YYYY')).format('DD-MM-YYYY'),
                    "date_end": Helpers.removeAndAddDayDate(12,moment(moment(passager[9],'DD-MM-YYYY'))),
                    "booking":Helpers.bookingRandom(),
                },
                hotel_data:{
                    "hotel":Helpers.calculateNoClient(10,28).toString(),
                    "hotel_country":passager[8],
                    "booking":Helpers.calculateNoClient(1431221,9935311).toString(),
                    "date_start": moment(moment(passager[9],'DD-MM-YYYY')).format('DD-MM-YYYY'),
                    "date_end": Helpers.removeAndAddDayDate(12,moment(moment(passager[9],'DD-MM-YYYY'))),
                    "hotel_address":Helpers.hotelAddressDocuments(passager[8]),
                    "hotel_name":Helpers.hotelNameDocuments()
                },
                banco_data:{
                    "cuenta":'DO'+Helpers.calculateNoClient(23186543,93186543).toString(),
                    "fecha_apertura":Helpers.removeAndAddDayToday(Helpers.calculateNoClient(-370,-650)),
                    "fecha_deposito":Helpers.removeAndAddDayToday(Helpers.calculateNoClient(-10,-45)),
                    "balance_actual":'RD$'+Helpers.calculateNoClient(300,900)+','+Helpers.calculateNoClient(56,900)+'.'+Helpers.calculateNoClient(10,99),
                    "balance_disponible":'RD$'+Helpers.calculateNoClient(300,900)+','+Helpers.calculateNoClient(56,900)+'.'+Helpers.calculateNoClient(10,99),
                    "promedio":'RD$'+Helpers.calculateNoClient(400,800)+','+Helpers.calculateNoClient(56,900)+'.'+Helpers.calculateNoClient(10,99),
                    "gerente":Helpers.personaRandomName(),
                    "fecha_doc":Helpers.removeAndAddDayToday(Helpers.calculateNoClient(-4,-10)),
                    "destinatario":'ISLAS VIRGENES BRITANICA',
                    "banco":Helpers.bancoRandom(),
                    "dateRequestEng":dateRequestEng
                },
                certificado_medico_data:{
                    "doctor":Helpers.personaRandomName(),
                    "exequartur":Helpers.calculateNoClient(13432,98590),
                    "edad_paciente":Helpers.calcularEdad(passager[5]),
                    "date_request":Helpers.removeAndAddDayToday(Helpers.calculateNoClient(-4,-10)),
                    "address_hospital":Helpers.hospitalRandom(),
                    "dateRequestEng":dateRequestEng

                },
                buena_conducta_data:{
                    "date_request":dateRequesBuenaConducta.format("DD-MM-YYY"),
                    "cis":Helpers.calculateNoClient(231,876) + '-'+Helpers.calculateNoClient(3200,8900)+'-'+Helpers.calculateNoClient(2837485,9837485)+'-'+Helpers.calculateNoClient(1,9),
                    "cas":Helpers.calculateNoClient(10754221900163,80754221900163),
                    "dateRequestForWritten": dateRequestSpa,
                    "dateRequestEng":dateRequestEng
                },
                carta_trabajo_data:{
                    "position":Helpers.positionRandom(),
                    "company":Helpers.companies(passager[13]),
                    "date_start":Helpers.removeAndAddDayToday(Helpers.calculateNoClient(-500,-900)),
                    "depertament":Helpers.departamentRandom(),
                    "salario":'RD$'+Helpers.calculateNoClient(50,150)+','+Helpers.calculateNoClient(100,955)+'.'+Helpers.calculateNoClient(10,60),
                    "date_request":Helpers.removeAndAddDayToday(Helpers.calculateNoClient(-10,-16)),
                    "gerente":Helpers.personaRandomName(),
                    "destinatario":'CONSULADO DE '+passager[8],
                    "tell_departament":Helpers.calculateNoClient(8092224311,8099224311).toString(),
                    "dateRequestEng":dateRequestEng,
                    "date_request":Helpers.removeAndAddDayToday(Helpers.calculateNoClient(-4,-10)),
                    "dateRequestSpa":dateRequestSpa

                }
               
            }

    }
}








module.exports = dataDocuments;