const puppeteer = require("puppeteer");
const moment = require("moment");




async function main(data) {
    const result =   data.pasajeros;
    const browser = await puppeteer.launch({
        headless: true,
        defaultViewport: null,});

    const page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 768});
    await  page.goto("https://sre.gt/es/", {waitUntil: 'networkidle0'});
    await page.waitForSelector("#txtfecha");
    // const daySelector = '.form-group';
    // await page.waitForSelector(daySelector);

    await page.$eval("#txtfecha",  (el,result)=>{
        el.value = result.fecha_entrada;
    },result);
    await page.keyboard.press(String.fromCharCode(13));
    await page.select('#txtaerolinea',result.aerolinea)
    await page.type("#txtnumero_vuelo", result.numero_vuelo)
    await page.type("#txtnumero_asiento", result.asiento)
    await page.select('#txtprocedencia',result.pais)

    await page.$eval("#txtfecha_programada_salida",  (el,result)=>{
        el.value = result.fecha_salida;
    },result);
    await page.type("#txtfecha_programada_salida", result.numero_vuelo)
    await page.keyboard.press(String.fromCharCode(13));
    await page.type("#txtnombre", result.nombre)
    await page.type("#txtapellido", result.apellidos)
    await page.select("#txtsexo", result.sexo)

    await page.$eval("#txtmenor_edad_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.type("#txtpasaporte", result.passaporte.toString())
    await page.select('#txtnacionalidad',result.nacionaldiad)
    await page.type("#txtprofesion",result.profesion )
    await page.type("#txtemail", result.email)

    await page.$eval("#txttelefono_codigo",(el,result) => {
        let options = el.options
        for (const i in options) {
            if(options[i].text ==result.codigo_cel){
                options[i].selected = true;
                el.options.selectedIndex = i
                let span = document.getElementById("select2-txttelefono_codigo-container");
                span.title = result.codigo_cel
                span.innerText = result.codigo_cel
                break
            }
        }
    },result);
    await page.type("#txttelefono", result.celular)

    await page.$eval("#txttel_emg_codigo",(el,result) => {
        let options = el.options
        for (const i in options) {
            if(options[i].text ==result.codigo_tel){
                options[i].selected = true;
                el.options.selectedIndex = i
                let span = document.getElementById("select2-txttel_emg_codigo-container");
                span.title = result.codigo_tel
                span.innerText = result.codigo_tel
                break
            }
        }
    },result);

    await page.type("#txttel_emg", result.telefono)
    await page.type("#txtdireccion", result.hotel_name+', '+result.hotel_direccion)

    await page.$eval("#txtviaja_solo_s",  (el,result)=>{
        el.checked = true;
        el.onchange();
    },result);

    await page.select('#txtviaje_motivo',result.motivo_viaje)
    await page.select('#txtviaje_pais_procedencia',result.pais)
    await page.select('#txtviaje_via_transporte',result.via)
    let date = moment(result.fecha_entrada, 'DD-MM-YYYY').format('YYYY-MM-DD')
    let date1 = moment(result.fecha_salida, 'DD-MM-YYYY').format('YYYY-MM-DD')
    let fecha1 = moment(date)
    let fecha2 = moment(date1)

    let diff = fecha2.diff(fecha1, 'days')
    await page.type("#txtviaje_dias",diff.toString() )

    await page.$eval("#txtcaso_coronavirus_contacto_n",  (el,result)=>{
        el.checked = true;
    },result);
    await page.$eval("#txtcaso_coronavirus_acomp_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txttemperatura_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txtcaso_coronavirus_acomp_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txttos_estornudos_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txtdolor_garganta_n",  (el,result)=>{
        el.checked = true;
    },result);


    await page.$eval("#txtperdida_gusto_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txtperdida_olfato_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txtdiarrea_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txtmalestar_n",  (el,result)=>{
        el.checked = true;
    },result);


    await page.$eval("#txtdificultad_respiratoria_n",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txtpcr_prueba_s",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval("#txtpcr_prueba72h_s",  (el,result)=>{
        el.checked = true;
    },result);

    await page.select('#txtcovid_tipo_prueba',"PCR")

    await page.$eval("#txtpcr_resultado_n",  (el,result)=>{
        el.checked = true;
        el.click();
    },result);

    await page.$eval("#txtpcr_prueba_aeropuerto_s",  (el,result)=>{
        el.checked = true;
    },result);

    await page.$eval( 'button#submit', form => form.click() );

    await page.waitForSelector(".text-center");
    await page.waitFor(4000);
    let path = 'pasajeros/'+result.nombre+' '+result.apellidos;
    try {
        const pdf = await page.pdf({ path: path+'/'+result.nombre+' '+result.apellidos+' - salud.pdf', printBackground: true });
    }catch (e) {
        console.log("error>: ",e)
    }
    //browser.close()
    console.log('TERMINE CON: '+result.nombre+' '+result.apellidos+'....')


}

module.exports = {
    main
}
