const moment =  require("moment");

const schema = {
    'nombres': {
        prop: 'nombres',
        type: String
    },
    'apellidos': {
        prop: 'apellidos',
        type: String
    },
    'cedula': {
        prop: 'cedula',
        type: String
    },
    'pasaporte': {
        prop: 'pasaporte',
        type: String
    },
    'nacimiento': {
        prop: 'nacimiento',
        type: String
    },
    'sexo': {
        prop: 'sexo',
        type: String
    },
    'destino': {
        prop: 'destino',
        type: String
    },
    'aerolinea': {
        prop: 'aerolinea',
        type: String
    },
    'novuelo': {
        prop: 'novuelo',
        type: String
    },
    'entrada': {
        prop: 'entrada',
        type: String
    },
    'salida': {
        prop: 'salida',
        type:  String
    },
    'pcr': {
        prop: 'pcr',
        type:  String
    },
    'hotel': {
        prop: 'hotel',
        type:  String
    },
    'pase': {
        prop: 'pase',
        type: (value) => {
            const bool = value.toLowerCase()
            if (bool === 'si') {
                return true
            }
            return false
        }
    },
    'resultado': {
        prop: 'resultado',
        type: (value) => {
            const bool = value.toLowerCase()
            if (bool === 'si') {
                return true
            }
            return false
        }
    },
    'hora': {
        prop: 'hora',
        type:  String
    },
    'seguro': {
        prop: 'seguro',
        type:  String
    },
    'ticket': {
        prop: 'ticket',
        type:  String
    },

}


module.exports = schema;
