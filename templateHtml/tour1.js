const html = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>property</title>
</head>
<body>

    <div class="contenedor_gnal">
        <div class="contenedor1">
            <div class="contenedor2">
                <div class="contenedor_inf">
                    <div class="nombre">
                        <h2>Itinerario</h2>
                        <h2>de Tours</h2>
                        <p>Nombre: Josue Cabrera</p>
                        <span>Duracion del viajes: 5 dias</span>
                    </div>
                    <div class="logo">
                        <div class="logo_inf">
                            <span>Aviarter</span>
                            <span>Tour</span>
                        </div>
                    </div>
                </div>
                <div class="contenedor_dias">
                    <div class="dias">
                        <span class="tour">TOUR</span>
                        <span>01</span>
                    </div>
                    <div class="contenedor_intinerario">
                        <h5>Destino: <span>Hotel(Room222)</span></h5>
                        <h5>Comer: <span>Local Restauran</span></h5>
                        <h5>Ocio: <span>Music Festival</span></h5>
                        <h5>Viajes: <span>Public transportation and cars</span></h5>
                    </div>
                </div>

                <div class="contenedor_dias">
                    <div class="dias">
                        <span class="tour">TOUR</span>
                        <span>01</span>
                    </div>
                    <div class="contenedor_intinerario">
                        <h5>Destino: <span>Hotel(Room222)</span></h5>
                        <h5>Comer: <span>Local Restauran</span></h5>
                        <h5>Ocio: <span>Music Festival</span></h5>
                        <h5>Viajes: <span>Public transportation and cars</span></h5>
                    </div>
                </div>

                <div class="contenedor_dias">
                    <div class="dias">
                        <span class="tour">TOUR</span>
                        <span>01</span>
                    </div>
                    <div class="contenedor_intinerario">
                        <h5>Destino: <span>Hotel(Room222)</span></h5>
                        <h5>Comer: <span>Local Restauran</span></h5>
                        <h5>Ocio: <span>Music Festival</span></h5>
                        <h5>Viajes: <span>Public transportation and cars</span></h5>
                    </div>
                </div>

                <div class="contenedor_dias">
                    <div class="dias">
                        <span class="tour">TOUR</span>
                        <span>01</span>
                    </div>
                    <div class="contenedor_intinerario">
                        <h5>Destino: <span>Hotel(Room222)</span></h5>
                        <h5>Comer: <span>Local Restauran</span></h5>
                        <h5>Ocio: <span>Music Festival</span></h5>
                        <h5>Viajes: <span>Public transportation and cars</span></h5>
                    </div>
                </div>

                <div class="contenedor_dias">
                    <div class="dias">
                        <span class="tour">TOUR</span>
                        <span>01</span>
                    </div>
                    <div class="contenedor_intinerario">
                        <h5>Destino: <span>Hotel(Room222)</span></h5>
                        <h5>Comer: <span>Local Restauran</span></h5>
                        <h5>Ocio: <span>Music Festival</span></h5>
                        <h5>Viajes: <span>Public transportation and cars</span></h5>
                    </div>
                </div>

                <div class="informacion_detalle">
                    <span>Septiembre 2023</span>
                    <span>7 Dias y 6 Noches</span>
                    <span>Puntos destacados: lago Tikitaki</span>
                    <span>Periodo de viaje: 19 de Septiembre - 01 Ocutbre </span>
                    <span>Tour Privado</span>
                </div>
            </div>
        </div>
    </div>

    <style>
                *{
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }

        .contenedor_gnal{
            display: flex;
            width: 100%;
            height: auto;
            align-items: center;
            justify-content: center;
        }

        .contenedor1{
            display: flex;
            align-items: center;
            justify-content: center;
            width: 37%;
            height: 900px;
            margin-top: 30px;
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        }

        .contenedor2{
            display: flex;
            flex-direction: column;
            width: 96%;
            height: 91vh;
            background-color: #87bfdc;
        }

        .contenedor_inf{
            display: flex;
            width: 100%;
            justify-content: space-between;
            padding: 20px;
        }

        .nombre{
            display: flex;
            flex-direction: column;
        }

        .nombre h2{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 30px;
            color: #f56510;
            font-weight: 700;
            text-shadow: 2px 2px #fff;
        }

        .nombre span{
            color: #fff;
            font-size: 12px;
            font-family: Arial, Helvetica, sans-serif;
            margin-top: 7px;
        }

        .nombre p{
            color: #fff;
            font-size: 18px;
            font-family: Arial, Helvetica, sans-serif;
            margin-top: 7px;
            margin-bottom: -5px;
            margin-top: 20px;
        }

        .logo{
            display: flex;
            width: 80px;
            padding: 5px;
            background: #f56510;
            text-align: justify;
            height: 100px;
            position: absolute;
            right: 32.8%;
            top: 30px;
            border: 0;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .logo_inf{
            display: flex;
            width: 100px;
            height: 100px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            border: solid 10px #fff dotted ;
        }

        .logo span{
            color: #fff;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: 15px;
        }



        .contenedor_dias{
            display: flex;
            width: 100%;
            justify-content: flex-end;
            margin-bottom: 10px;
            gap: 6px;
        }

        .dias{
            display: flex;
            width: 130px;
            height: 100px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            background: #ecb60e;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
            position: absolute;
            left: 31.5%;
        }

        .dias span{
            color: #fff;
            font-size: 60px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
        }

        .tour{
            font-size: 18px!important;
            font-weight: 100!important;
            margin-bottom: -5px;
            font-family: Arial, Helvetica, sans-serif; 
        }

        .contenedor_intinerario{
            display: flex;
            flex-direction: column;
            width: 90%;
            height: 100px;
            background: #fff;
            justify-content: center;
            margin-right: 10px;
            padding: 10px;
            gap: 8px;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
            border: 0;
            margin-left: 123px;
        }

        .contenedor_intinerario h5{
            display: flex;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            margin-left: 10px;
        }

        .contenedor_intinerario span{
            display: flex;
            font-family: Arial, Helvetica, sans-serif;
            margin-left: 10px;
            font-weight: 100;
        }

        .informacion_detalle{
            display: flex;
            width: 35.5%;
            height: 100px;
            flex-direction: column;
            padding: 30px;
            gap: 5px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #fff;
            justify-content: center;
            background: #f56510;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
            position: absolute;
            bottom: 100px;
            left: 31.5%;
        }
    </style>

</body>
</html>
`