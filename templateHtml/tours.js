const tours = [
    {
        "nameTour":"Tour Termal : Café Albania + Aguas Termales + Ataco + Almuerzo",
        "timeTOur":"de 7 a 10 horas",
        "placeTOur":"San salvador",
        "eatTOur":"Tour Termal : Café Albania + Aguas Termales + Ataco + Almuerzo",
        "descrTOur":"Cambie las bulliciosas calles de la ciudad de San Salvador por el esplendor natural de las cascadas Salto de Malacatiupan y las aguas termales de Santa Teresa en una excursión privada de un día que le ahorra la molestia de alquilar un automóvil o tomar el transporte público; en cambio, puede sentarse y relajarse como otra persona conduce. Además, un grupo pequeño limitado a 10 significa que puede explorar cada atracción a un ritmo relajado."
    },
    {
        "nameTour":"Mejor excursión de un día: caminata por el volcán Santa Ana + vista panorámica del lago de Coatepeque",
        "timeTOur":"de 7 a 10 horas",
        "placeTOur":"San salvador",
        "eatTOur":"Almuerzo",
        "descrTOur":"Los viajeros activos y amantes del aire libre disfrutarán de la oportunidad de caminar hasta la cima de un volcán sin perderse. La logística estará a cargo, así que todo lo que tiene que hacer es caminar. La subida a Ilamatepec toma unas dos horas, y la bajada otras dos. Sorpréndase con las vistas desde la cima, incluida la laguna del cráter verde. Relájese con un almuerzo tranquilo en el lago Coatepeque."
    },
    {
        "nameTour":"Lo mejor de San Salvador en 1 día: El Boquerón + Tobogán Arco Iris + Centro Histórico",
        "timeTOur":"6 horas",
        "placeTOur":"San salvador",
        "eatTOur":"no tiene",
        "descrTOur":"Explore el Parque Nacional El Boquerón y la ciudad de San Salvador en este tour combinado de todo el día. Primero, diríjase al parque nacional, que se encuentra en la cima del Volcán de San Salvador, y deténgase en la atracción principal: un cráter que mide 3.1 millas (cinco kilómetros) de diámetro y 1,831 pies (558 metros) de profundidad. Desde allí, regrese a la ciudad para visitar lugares importantes, como el Monumento a la Revolución, el Parque Cuscatlán, la Catedral Metropolitana y más."
    },
    {
        "nameTour":"Excursión de 2 visitas turísticas diferentes en 1 día: Parque de los Volcanes y dos sitios mayas",
        "timeTOur":"de 8 a 9 horas",
        "placeTOur":"San salvador",
        "eatTOur":"Desayuno + Almuerzo",
        "descrTOur":"No dejes que el tiempo limitado te impida ver lo mejor de San Salvador. Marque dos sitios arqueológicos mayas, San Andrés y Tazumal, más el Parque Nacional Cerro Verde en un recorrido privado que le brinda la flexibilidad de personalizar su itinerario. Además, dejar que otra persona conduzca le permite sentarse y disfrutar del paisaje"
    },
    {
        "nameTour":"Tour de día completo: las ruinas de Copán, un increíble sitio maya desde la ciudad de San Salvador",
        "timeTOur":"de 12 a 15 horas",
        "placeTOur":"San salvador",
        "eatTOur":"Almuerzo",
        "descrTOur":"Aproveche la proximidad de El Salvador a las ruinas mayas de Copán en una excursión privada de un día al sitio del Patrimonio Mundial de la UNESCO en Honduras. Su guía lo ayudará a cruzar la frontera sin problemas y le brindará toda su atención mientras explora el sitio arqueológico a un ritmo establecido por usted en lugar de un grupo grande."
    },
    {
        "nameTour":"Ruta de las Flores: Aguas Termales + Tour del Café + Pueblos de Nahuizalco y Ataco",
        "timeTOur":"de 7 a 8 horas",
        "placeTOur":"San salvador",
        "eatTOur":"Almuerzo",
        "descrTOur":"Explore una de las áreas más populares de El Salvador acompañado de un guía que puede hablar el idioma y contar historias que no encontrará en la guía. Durante este recorrido privado por la Ruta de las Flores, se beneficiará de la atención exclusiva de su guía privado mientras descubre el pueblo indígena de Nahuizalco, las aguas termales de Santa Teresa y el centro cultural de Ataco."
    },
    {
        "nameTour":"Sitio Joya de Cerén + Sitio Ceremonial Tazumal + Taller Índigo",
        "timeTOur":"de 6 a 8 horas",
        "placeTOur":"San salvador",
        "eatTOur":"snack + Almuerzo",
        "descrTOur":"A los viajeros interesados en la cultura y la historia les encantará esta excursión de un día que se centra en los indígenas de El Salvador, los mayas. Aprenderá todo sobre la historia de El Salvador en los impresionantes restos arqueológicos de Joya de Ceren y Tazumal. También haga una parada para almorzar en el hermoso lago de Coatepeque. Este tour es una manera fácil de ver algunos de los tesoros culturales de El Salvador en un día."
    },
    {
        "nameTour":"Explore el complejo de cascadas de Tamanique + Surf City Playa El Tunco",
        "timeTOur":"de 5 a 7 horas",
        "placeTOur":"San salvador",
        "eatTOur":"Almuerzo",
        "descrTOur":"La ciudad capital de El Salvador está a poca distancia en automóvil de la playa, así que aproveche este conveniente recorrido para explorar el paisaje natural del país. Antes de llegar a la costa, se detendrá en las Cascadas de Tamanique, una serie de tres cascadas a las que caminará por una empinada subida. Use su traje de baño para saltar a las piscinas de primavera y luego llegue a Playa El Tunco antes de secarse. Pasa el rato en este pueblo de surf o simplemente relájate en la arena."
    },
    {
        "nameTour":"Tour combinado: Volcán San Salvador (El Boquerón) y colorido pueblo de Suchitoto",
        "timeTOur":"6 horas",
        "placeTOur":"San salvador",
        "eatTOur":"desayuno",
        "descrTOur":"Salga de San Salvador y verá que hay docenas de reservas naturales y pueblos coloridos para explorar en El Salvador. Esta excursión de un día desde la capital destaca dos atracciones populares cercanas. Primero, visitarás el volcán San Salvador, que es uno de los más accesibles del país. Simplemente camine por un sendero y llegará al cráter en cuestión de minutos. Luego, visitarás Suchitoto, un pueblo bien conservado conocido por su cultura."
    },
    {
        "nameTour":"Tour Termal : Café Albania + Aguas Termales + Ataco + Almuerzo",
        "timeTOur":"de 7 a 10 horas",
        "placeTOur":"San salvador",
        "eatTOur":"Almuerzo",
        "descrTOur":"Cambie las bulliciosas calles de la ciudad de San Salvador por el esplendor natural de las cascadas Salto de Malacatiupan y las aguas termales de Santa Teresa en una excursión privada de un día que le ahorra la molestia de alquilar un automóvil o tomar el transporte público; en cambio, puede sentarse y relajarse como otra persona conduce. Además, un grupo pequeño limitado a 10 significa que puede explorar cada atracción a un ritmo relajado."
    },
    {
        "nameTour":"Día completo Ruta de Las Flores visita cuatro pueblos coloniales y cafetaleros.",
        "timeTOur":"de 8 a 9 horas",
        "placeTOur":"San salvador",
        "eatTOur":"Tour Termal : Café Albania + Aguas Termales + Ataco + Almuerzo",
        "descrTOur":"Cambie las bulliciosas calles de la ciudad de San Salvador por el esplendor natural de las cascadas Salto de Malacatiupan y las aguas termales de Santa Teresa en una excursión privada de un día que le ahorra la molestia de alquilar un automóvil o tomar el transporte público; en cambio, puede sentarse y relajarse como otra persona conduce. Además, un grupo pequeño limitado a 10 significa que puede explorar cada atracción a un ritmo relajado."
    },
    {
        "nameTour":"Tour Termal : Café Albania + Aguas Termales",
        "timeTOur":"de 5 horas",
        "placeTOur":"San salvador",
        "eatTOur":"desayuno",
        "descrTOur":"Cambie las bulliciosas calles de la ciudad de San Salvador por el esplendor natural de las cascadas Salto de Malacatiupan y las aguas termales de Santa Teresa en una excursión privada de un día que le ahorra la molestia de alquilar un automóvil o tomar el transporte público; en cambio, puede sentarse y relajarse como otra persona conduce. Además, un grupo pequeño limitado a 10 significa que puede explorar cada atracción a un ritmo relajado."
    }
]

module.exports = {
    tours
}
