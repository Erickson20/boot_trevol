
const e = require("express");
const moment =  require("moment");
const QRCode  =  require('qrcode');
const fs = require('fs');
const tours = require('./templateHtml/tours')








function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


function bancoRandom(params) {
    let results = ['BANCO POPULAR','BANRESERVAS','BHD LEON','ASOCIACION POPULAR']
    return results[Math.floor(results.length * Math.random())];
}

function companies(no) {
    console.log('no :>> ', no);
    switch (no) {
        case 1:
           return 'SUPRAMAX INDUSTRIAL '
        case 2:
            return 'IMPORTADORAS BRETEL '
        case 3:
            return 'INDUSTRIAS CAMAYO SRL'
        case 4:
            return 'CONSTRUCTORA VIGAS ALPHA'
        case 5:
            return 'NOVA SYSTEMS'   
    }
}


function profesionRandom() {
    let results = ['VENDEDOR', 'PELUQUERO', 'PRESTAMISTA', 'EMPLEADO PUBLICO', 'EMPLEADO PRIVADO', 'COMERCIANTE'];
    return results[Math.floor(results.length * Math.random())];
}

function positionRandom() {
    let results = ['SUPERVISOR', 'SUPERVISOR EJECUTIVO', 'GERENTE','GERENTE DE DEPARTAMETNO','ADMINISTRADOR','TEAM LEADER','ENCARGADO DE SEGURIDAD','ENCARGADO DE TECNOLOGIA','SUPERVISOR DE PISO','ENCARGADO DE CALIDAD','GERENTE DE RRHH','GERENTE DE VENTA','ADMINISTRADOR DE CALIDAD','ENCARGADO DE PRODUCCION',' SUPERVISOR DE CALIDAD DE PRODUCTO']
    return results[Math.floor(results.length * Math.random())];
}

function departamentRandom() {
    let results = ['INNOVACION','VENTA','RECURSOS HUMANOS','ALMACEN','CALIDAD','COMPRAS','SOLICITUDES','FINANZAS','MARKETING','LOGISTICA','GESTION','TRANSPORTE']
    return results[Math.floor(results.length * Math.random())];
}

function email(name,lastname) {
    let email = ['@hotmail.com', '@gmail.com', '@outlook.com'];
    let selectedEmail = email[Math.floor(email.length * Math.random())];

    return name.split(" ")[0].toLowerCase()+lastname.split(" ")[0].toLowerCase()+selectedEmail;
}


function celularRandom() {
    let results = ['8097654388', '8091236744', '8491964388', '8097895433', '8096758322', '8298760211'];
    return results[Math.floor(results.length * Math.random())];
}

function vendedorRandom() {
    let results = ['Julio Nova', 'Ramon Perez', 'Steven Lower', 'Nelson Alcantara', 'Martin Perez', 'Gregori Roman'];
    return results[Math.floor(results.length * Math.random())];
}

function contactoRandom() {
    let results = ['Mario Hernandez', 'Luisa Rodriguez', 'Stephanie Pilin', 'Josefina Lomber', 'Keysi Bisono', 'Juan Checo'];
    return results[Math.floor(results.length * Math.random())];
}

function telefonoRandom() {
    let results = ['8097224311', '8091846743', '8491874312', '8095495490', '8096747331', '8298711220'];
    return results[Math.floor(results.length * Math.random())];
}
function numberFlightRandom() {
    let results = ['1347826870308', '1347826870308', '1347826870308', '1347826870308', '1347826870308', '1347826870308'];
    return results[Math.floor(results.length * Math.random())];
}

function doctorRandom() {
    let results = ['87654', '65321', '98762', '12764', '90876', '35217'];
    return results[Math.floor(results.length * Math.random())];
}

function bookingRandom() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  
    for (var i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
}

function dayForWeek(day) {
        switch (day) {
            case 1:
                return 'Lun'
            case 2:
                return 'Mar'
            case 3:
                return 'Mie'
            case 4:
                return 'Jue'
            case 5:
                return 'Vie'
            case 6:
                return 'Sab'
            case 7:
                return 'Dom'
            case 0:
                return 'Dom'

            default:
                return 'NaN'
               
        }
}

function doctorNameRandom() {
    let results = ['DR. RAMON GARCIA PEREZ', 'DR. FRANCIS GERMAN PAULINO', 'DR. JOSUE LORENZO MARTINEZ', 'DR. ROBERTO LOPEZ FLORES', 'DRA. ASHLEY GUZMAN ROSARIO', 'DRA. MARIA TORRES BISONO'];
    return results[Math.floor(results.length * Math.random())];
}

function hospitalRandom() {
    let results = ['Hospital Nuestra Señora De Regla', 'Clinica Peravia', 'Hospital Municipal Villa Fundación', 'Laboratorio Metra Banl', 'SAN LORENZO DE LOS MINA MATERNO INFANTIL'];
    return results[Math.floor(results.length * Math.random())].toUpperCase();
}


function hotelDireccionRandom() {
    let results = [
        '2172 14 Avenida, Zona 13, Perrie, San patricio, 01014 ',
        '8a. avenida 15-85 Zona 13 Colonia Aurora I, Zona 13, Verde, 01013', 
        '4-39 15 Avenida A 15 Avenida “A” 4-39 Zona 14', 
        '16 calle 7-40 centro, Aurora 1', '16 calle 7-40 capitilio, diagonal 26 19-18',
        'Blvd. Costa del Sol, San Luis la Herradura',
        'Blvd. Costa del Sol km 66.5, San Luis la Herradura',
        'KM 40.5 Carretera al Aeropuerto de, San Luis Talpa',
        'Km. 40 Carretera Aeropuerto Internacional de Comalpa, Ex-Peaje, San Luis Talpa'
    ];
    return results[Math.floor(results.length * Math.random())].toUpperCase();
}

function hotelNameDocuments() {
    let results = [
        'Sun Fun Resort',
        'Hillcrest',
        'The Oasis Retreat',
        'Holiday',
        'More Than Beauty Properties',
        'Long Bay Beach Resort',
        'Abigails Spectacular '
    ]
    return results[Math.floor(results.length * Math.random())].toUpperCase();
}

function hotelAddressDocuments(country) {
    let results = [
        'Mayaba Estate, VG1110 ',
        'West Bay Street 627',
        'Sears Hill Road',
        'Lember Wset street 234',
        'Long Bay Beach Resort West End, VG1130',
        'Hemaway Longscape street 122',
        'EST av bulloc no 456'
    ]
    return results[Math.floor(results.length * Math.random())].toUpperCase() +', '+country;
}


function addressPersonalRandom(country) {
    let sector = ['La pinta', 'El barril', 'Villa Duarte', 'Miramar', 'Miraflores', 'Villa Faro', 'Mendoza', 'Sanchez', 'Gregorio Luperon'];
    let calle = ["jimenez 14", "princesa limbar","Chales de gaulles","robert pastor","terminal 14","calle vasquez 23","calle paseo de las casas"," avenida Duarte","avenida Juan Calamar"," av 50 esquina chuz","calle monumentos","carretera jimenoa","carr. los coroneles","carr. san isidro","calle los pinos","calle piso 17","av pintura","calle camino","avenida mella","calle san vicente","calle patio atras","respaldo los prados","residencial leon calle carpintero","avenida lobito 13"];
    let no = ["casa no 23","casa no 8","apt no 23","apt no 10","apt no 65","casa no 90","casa no 43"," apartamento 87"," apartamento 34"," apartamento 98"," apartamento 31","casa 11"," casa no 41","apartamento 21","casa no 86","casa no 49","casa 12","apt 22","apt 67","apt 88","apt 99"," casa 18","casa 19"]
    let sectorRandom = sector[Math.floor(sector.length * Math.random())]
    let calleRandom = calle[Math.floor(calle.length * Math.random())]
    let noRandom = no[Math.floor(no.length * Math.random())]
    return sectorRandom +', '+ calleRandom +', '+ noRandom + ', ' + country
}


function personaRandomName() {
    let apellidos = ['Flores','Vizcaino','Perez','Garcia','Rodriguez','Bisono','Cifre','Mejia','Romero','Aybar','Sanchez','De La Rosa','Polanco']
    let nombresM = ['Robison','Ramon','Juan','Pablo','Nelson','Luis','Yordani','Erick','Romer','Josue','Frank','Emmanuel','Alfonso']
    let nombresF = ['Julieta','Maria','Nicaury','Juana','Stefany','Petra','Luisa','Josefina','Julissa','Leidy','Francelis','Marta','Talia']
    let allNames = nombresM.concat(nombresF);
    return [ 
        allNames[Math.floor(nombresF.length * Math.random())], 
        apellidos[Math.floor(apellidos.length * Math.random())],
        apellidos[Math.floor(apellidos.length * Math.random())] 
    ].toString().toUpperCase().replaceAll(","," ")
}

function parentRandomName(lastname) {
    let apellidos = ['Flores','Vizcaino','Perez','Garcia','Rodriguez','Bisono','Cifre','Mejia','Romero','Aybar','Sanchez','De La Rosa','Polanco']
    let nombresM = ['Robison','Ramon','Juan','Pablo','Nelson','Luis','Yordani','Erick','Romer','Josue','Frank','Emmanuel','Alfonso']
    let nombresF = ['Julieta','Maria','Nicaury','Juana','Stefany','Petra','Luisa','Josefina','Julissa','Leidy','Francelis','Marta','Talia']
    let allNames = nombresM.concat(nombresF);
    return [ 
        nombresM[Math.floor(nombresF.length * Math.random())], 
        apellidos[lastname.split(' ')[0]],
        apellidos[Math.floor(apellidos.length * Math.random())] 
    ].toString().toUpperCase()
}

function familyName(sex,lastname) {
    let apellidos = ['Flores','Vizcaino','Perez','Garcia','Rodriguez','Bisono','Cifre','Mejia','Romero','Aybar','Sanchez','De La Rosa','Polanco']
    let nombresM = ['Robison','Ramon','Juan','Pablo','Nelson','Luis','Yordani','Erick','Romer','Josue','Frank','Emmanuel','Alfonso']
    let nombresF = ['Julieta','Maria','Nicaury','Juana','Stefany','Petra','Luisa','Josefina','Julissa','Leidy','Francelis','Marta','Talia']
    let allNames = nombresM.concat(nombresF);
    let counterChildren = calculateNoClient(1,3);
    let allLastname = lastname.split(' ');
    let childrenInfo = []
    let coupleName = []
    let contactEmer = []

    if(sex === 'F'){
        coupleName = [ 
            nombresM[Math.floor(nombresM.length * Math.random())], 
            apellidos[Math.floor(apellidos.length * Math.random())],
            apellidos[Math.floor(apellidos.length * Math.random())] 
        ]
    }else{
        coupleName = [ 
            nombresF[Math.floor(nombresM.length * Math.random())], 
            apellidos[Math.floor(apellidos.length * Math.random())],
            apellidos[Math.floor(apellidos.length * Math.random())] 
        ]
    }

    contactEmer = [ 
        allNames[Math.floor(nombresF.length * Math.random())], 
        apellidos[Math.floor(apellidos.length * Math.random())],
        apellidos[Math.floor(apellidos.length * Math.random())] 
    ]

    for (let i = 0; i <= counterChildren; i++) {
        let namesCHilFull = ''
        if(sex === 'F'){
            namesCHilFull = allNames[Math.floor(nombresF.length * Math.random())] +' '+
                            coupleName[1] +' '+
                            allLastname[0] 
            childrenInfo.push(namesCHilFull)
        }else{
            namesCHilFull = allNames[Math.floor(nombresF.length * Math.random())] +' '+
                            allLastname[0] +' '+
                            coupleName[1] 
            childrenInfo.push(namesCHilFull)
        }
        
    }

    return {
        contact_emer: contactEmer.toString().toUpperCase().replaceAll(","," "),
        children: childrenInfo.toString().toUpperCase().replaceAll(",",", "),
        couple: coupleName.toString().toUpperCase().replaceAll(","," ")
    }
}

function hotelRandom(type) {
    let hotelsMex = ['Hostal CASA MX centro', 'Hotel MX lagunilla', 'Hotel Block Suites', 'Barceló Méx Reforma'];
    let hotelsSal = ['HOTEL TESORO BEACH', 'Costa Inn Hotel (Hotel Real Costa Inn)', 'Quality Hotel Real Aeropuerto', 'Argueta Hotel'];
    let hotelsGua = ['Holiday Inn', 'In and out Hotel', 'Hotel San Carlos', 'Radisson hotel','Hostal CASA centro','Hotel lagunilla','Hotel Block Suites','Barceló Reforma'];
    
      
    if(type.toLowerCase() === "mexico"){
        return {hotelName:hotelsMex[Math.floor(hotelsMex.length * Math.random())],direccion:hotelDireccionRandom()}
    }

    if(type.toLowerCase() === "guatemala"){
        return {hotelName:hotelsGua[Math.floor(hotelsMex.length * Math.random())],direccion:hotelDireccionRandom()}
    }

    if(type.toLowerCase() === "salvador"){
        return {hotelName:hotelsSal[Math.floor(hotelsMex.length * Math.random())],direccion:hotelDireccionRandom()}
    }

    return {hotelName:hotelsGua[Math.floor(hotelsMex.length * Math.random())] + ', '+type,direccion:hotelDireccionRandom()}

}

function calcularEdad(fecha) {
    let nacimiento=moment(fecha,'DD-MM-YYYY').format('YYYY-MM-DD')
    let hoy=moment();
    let anios=hoy.diff(nacimiento,"years");

    return anios;
}


function beforeHour(hour) {
    return moment(hour,"HH:MM").subtract(5, 'hours').format("HH:MM")
    
}

function today() {
    return moment().add(-1, 'days').format('DD-MM-YYYY')
}

function removeAndAddDayToday(days) {
    return moment().add(days, 'days').format('DD-MM-YYYY')

}

function removeAndAddDayTodayForWritten(days) {
    return moment().add(days, 'days')

}

function removeAndAddDayDate(days,date) {
    return date.add(days, 'days').format('DD-MM-YYYY')

}


function getDaysByTwoDate(fechaUno, fechaDos) {
    let date = moment(fechaUno, 'DD-MM-YYYY').format('YYYY-MM-DD')
    let date1 = moment(fechaDos, 'DD-MM-YYYY').format('YYYY-MM-DD')
    let fecha1 = moment(date)
    let fecha2 = moment(date1)

    let diff = fecha2.diff(fecha1, 'days')
    return diff;
}


function createTourTemplate(psg,template) {
    
   dates=[]
    console.log('object *********** :>> ',psg.pasajeros.fecha_entrada);
    startDate =  moment(psg.pasajeros.fecha_entrada, 'DD-MM-YYYY').add(1, 'days');

    while(startDate.format('DD-MM-YYYY') !== moment(psg.pasajeros.fecha_salida, 'DD-MM-YYYY').format('DD-MM-YYYY')) {
      console.log(moment(startDate.toDate(), 'DD-MM-YYYY').format('DD-MM-YYYY'));
      dates.push(moment(startDate.toDate(), 'DD-MM-YYYY').format('DD-MM-YYYY'));
      startDate = startDate.add(1, 'days');
    }

    let min = Math.ceil(4);
    let max = Math.floor(tours.tours.length);
    console.log('min,max :>> ', min,max);
    let numberTours = Math.floor(Math.random() * (max - min) + min);
    let randonTour = shuffle(tours.tours); 
    let finallyTour = randonTour.slice(0,numberTours)
    let finallyDate = dates.slice(0,numberTours)

    for (const key in finallyDate) {
        finallyTour[key].date = finallyDate[key]
    }
    let html = htmlBuild(psg.pasajeros,finallyTour)
    console.log('html 23423 23 23 4234 2 :>> ', html);
    return finallyTour

}








function htmlBuild(psg,finallyTour) {
    console.log('object :>> ', finallyTour);
    let infoTours = ""
    let c=1
    for (const i in finallyTour) {
        infoTours += `<div class="contenedor_dias">
                    <div class="dias">
                        <span class="tour">TOUR</span>
                        <span>${c++}</span>
                    </div>
                    <div class="contenedor_intinerario">
                        <h5>Destino: <span>${finallyTour[i].nameTour}</span></h5>
                        <h5>Incluido: <span>${finallyTour[i].eatTOur}</span></h5>
                        <h5>Tiempo: <span>${finallyTour[i].timeTOur}</span></h5>
                        <h5>Fecha: <span>${finallyTour[i].date}</span></h5>
                    </div>
                </div>`
                
    }
     
    let html = `
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>property</title>
</head>
<body>

    <div class="contenedor_gnal">
        <div class="contenedor1">
            <div class="contenedor2">
                <div class="contenedor_inf">
                    <div class="nombre">
                        <h2>Itinerario</h2>
                        <h2>de Tours</h2>
                        <p>Nombre: ${psg.nombre} ${psg.apellidos}</p>
                        <span>Total de tours: ${finallyTour.length}</span>
                    </div>
                    <div class="logo">
                        <div class="logo_inf">
                            <span>Aviarter</span>
                            <span>Tour</span>
                        </div>
                    </div>
                </div>
                
                ${infoTours}

                <div class="informacion_detalle">
                    <span>Puntos destacados en San Salvador</span>
                    <span>Periodo de viaje: ${psg.fecha_entrada} - ${psg.fecha_salida} </span>
                    <span>Tour Privado</span>
                </div>
            </div>
        </div>
    </div>

    <style>
                *{
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }

        .contenedor_gnal{
            display: flex;
            width: 100%;
            height: auto;
            align-items: center;
            justify-content: center;
        }

        .contenedor1{
            display: flex;
            align-items: center;
            justify-content: center;
            width: 37%;
            height: 900px;
            margin-top: 30px;
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        }

        .contenedor2{
            display: flex;
            flex-direction: column;
            width: 96%;
            height: 91vh;
            background-color: #87bfdc;
        }

        .contenedor_inf{
            display: flex;
            width: 100%;
            justify-content: space-between;
            padding: 20px;
        }

        .nombre{
            display: flex;
            flex-direction: column;
        }

        .nombre h2{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 30px;
            color: #f56510;
            font-weight: 700;
            text-shadow: 2px 2px #fff;
        }

        .nombre span{
            color: #fff;
            font-size: 12px;
            font-family: Arial, Helvetica, sans-serif;
            margin-top: 7px;
        }

        .nombre p{
            color: #fff;
            font-size: 18px;
            font-family: Arial, Helvetica, sans-serif;
            margin-top: 7px;
            margin-bottom: -5px;
            margin-top: 20px;
        }

        .logo{
            display: flex;
            width: 80px;
            padding: 5px;
            background: #f56510;
            text-align: justify;
            height: 100px;
            position: absolute;
            right: 32.8%;
            top: 30px;
            border: 0;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .logo_inf{
            display: flex;
            width: 100px;
            height: 100px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            border: solid 10px #fff dotted ;
        }

        .logo span{
            color: #fff;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: 15px;
        }



        .contenedor_dias{
            display: flex;
            width: 100%;
            justify-content: flex-end;
            margin-bottom: 10px;
            gap: 6px;
        }

        .dias{
            display: flex;
            width: 130px;
            height: 100px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            background: #ecb60e;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
            position: absolute;
            left: 31.5%;
        }

        .dias span{
            color: #fff;
            font-size: 60px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
        }

        .tour{
            font-size: 18px!important;
            font-weight: 100!important;
            margin-bottom: -5px;
            font-family: Arial, Helvetica, sans-serif; 
        }

        .contenedor_intinerario{
            display: flex;
            flex-direction: column;
            width: 90%;
            height: 100px;
            background: #fff;
            justify-content: center;
            margin-right: 10px;
            padding: 10px;
            gap: 8px;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
            border: 0;
            margin-left: 123px;
        }

        .contenedor_intinerario h5{
            display: flex;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            margin-left: 10px;
        }

        .contenedor_intinerario span{
            display: flex;
            font-family: Arial, Helvetica, sans-serif;
            margin-left: 10px;
            font-weight: 100;
        }

        .informacion_detalle{
            display: flex;
            width: 35.5%;
            height: 100px;
            flex-direction: column;
            padding: 30px;
            gap: 5px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #fff;
            justify-content: center;
            background: #f56510;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
            position: absolute;
            bottom: 100px;
            left: 31.5%;
        }
    </style>

</body>
</html>
    `


    var fileName = __dirname + '/pasajeros/'+psg.nombre+' '+psg.apellidos + '/ tours.html';
    fs.writeFileSync(fileName, html);
    infoTours = ""
    html = ""
    return html
}
const shuffle = (array) => { 
    return array.sort(() => Math.random() - 0.5); 
};

function getPriceByNight(allNight) {
    let night = [23.12,12.3,10.53,16.23,18.43,20.01,21.90]
    let nightRandom = night[Math.floor(night.length * Math.random())];
    let total = allNight * nightRandom;
    let tax = total *0.18
    return{
        nightPrice: nightRandom,
        tax: tax,
        total: total + tax
    }
}

function creditCard() {
    card = ['Visa 9543','MasterCard 2134','Visa 9854','MasterCard 4309','Visa 7854','MasterCard 2343','Visa 1987','MasterCard 0987','Visa 7689','MasterCard 4487',' Visa 3287','MasterCard 8909','Visa 4867','MasterCard 3546']
    return card[Math.floor(card.length * Math.random())];
}

function templateHotelRandom() {
    let hotel = ['hotel.docx', 'hotel3.docx', 'hotel2.docx'];

    return hotel[Math.floor(hotel.length * Math.random())];
}

function templateSeguroRandom() {
    let hotel = ['seguro.docx', 'seguro2.docx', 'seguro3.docx'];

    return hotel[Math.floor(hotel.length * Math.random())];
}


function qr(path,data) {
    QRCode.toFile(path+'/qr.png', data, {
        color: {
            dark: '#209248',  // Blue dots
            light: '#0000' // Transparent background
        }
    }, function (err) {
        if (err) throw err
    })
}


function calculateNoClient(inferior,superior) {
    let numPosibilidades = superior - inferior;
    let aleatorio = Math.random() * (numPosibilidades + 1);
    aleatorio = Math.floor(aleatorio);
    return inferior + aleatorio;
}

function restDays(date, days){
    let startdate = moment(date,'DD-MM-YYYY')
    let dateNew = startdate.subtract(days, 'd');
    return dateNew.format('DD/MM/YYYY');
}

function getAirline(type) {
    switch (type){
        case 'aeromexico':
            return "1"
        break;
        case 'copa':
            return "5"
        break;
        case 'avianca':
            return "3"
        break;
        default:
            return "1"
        break;
    }

}
const numbersToWords = {
    0: "zero",
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine",
    10: "ten",
    11: "eleven",
    12: "twelve",
    13: "thirteen",
    14: "fourteen",
    15: "fifteen",
    16: "sixteen",
    17: "seventeen",
    18: "eighteen",
    19: "nineteen",
    20: "twenty",
    30: "thirty",
    40: "forty",
    50: "fifty",
    60: "sixty",
    70: "seventy",
    80: "eighty",
    90: "ninety",
  };

  const numbersToWordsSpa = {
    0: "cero",
    1: "uno",
    2: "dos",
    3: "tres",
    4: "cuatro",
    5: "cinco",
    6: "seis",
    7: "siete",
    8: "ocho",
    9: "nueve",
    10: "diez",
    11: "once",
    12: "doce",
    13: "trece",
    14: "catorce",
    15: "quince",
    16: "dieciseis",
    17: "diecisiete",
    18: "dieciocho",
    19: "diecinueve",
    20: "veinte",
    30: "treinta",
    40: "cuarenta",
    50: "cincuenta",
    60: "sesenta",
    70: "setenta",
    80: "ochenta",
    90: "noventa",
  };

// Define the convertNumberToWords function
function convertNumberToWords(number, esp = false) {
    // if number present in object no need to go further
    if (number in numbersToWords) return numbersToWords[number];
  
    // Initialize the words variable to an empty string
    let words = "";
  
    // If the number is greater than or equal to 100, handle the hundreds place (ie, get the number of hundres)
    if (number >= 100) {
      // Add the word form of the number of hundreds to the words string
      words += convertNumberToWords(Math.floor(number / 100)) + " hundred";
  
      // Remove the hundreds place from the number
      number %= 100;
    }
  
    // If the number is greater than zero, handle the remaining digits
    if (number > 0) {
      // If the words string is not empty, add "and"
      if (words !== "") words += " and ";
  
      // If the number is less than 20, look up the word form in the numbersToWords object
      if (number < 20) words += numbersToWords[number];
      else {
        // Otherwise, add the word form of the tens place to the words string
        //if number = 37, Math.floor(number /10) will give you 3 and 3 * 10 will give you 30
        words += numbersToWords[Math.floor(number / 10) * 10];
  
        // If the ones place is not zero, add the word form of the ones place
        if (number % 10 > 0) {
          words += "-" + numbersToWords[number % 10];
        }
      }
    }
  
    // Return the word form of the number
    return words;
}
  function convertNumberToWordsSpa(number, esp = false) {
    // if number present in object no need to go further
    
    if (number in numbersToWordsSpa) return numbersToWordsSpa[number];
  
    // Initialize the words variable to an empty string
    let words = "";
  
    // If the number is greater than or equal to 100, handle the hundreds place (ie, get the number of hundres)
    if (number >= 100) {
      // Add the word form of the number of hundreds to the words string
      words += convertNumberToWordsSpa(Math.floor(number / 100)) + " ciento";
  
      // Remove the hundreds place from the number
      number %= 100;
    }
  
    // If the number is greater than zero, handle the remaining digits
    if (number > 0) {
      // If the words string is not empty, add "and"
      if (words !== "") words += " and ";
  
      // If the number is less than 20, look up the word form in the numbersToWords object
      if (number < 20) words += numbersToWordsSpa[number];
      else {
        // Otherwise, add the word form of the tens place to the words string
        //if number = 37, Math.floor(number /10) will give you 3 and 3 * 10 will give you 30
        words += numbersToWordsSpa[Math.floor(number / 10) * 10];
  
        // If the ones place is not zero, add the word form of the ones place
        if (number % 10 > 0) {
          words += "-" + numbersToWordsSpa[number % 10];
        }
      }
    }
  
    // Return the word form of the number
    return words;
  }


  function mountSpa(m) {
      switch (m) {
        case 'January':
            return 'Enero'
        case 'February':
            return 'Febrero'
        case 'March':
            return 'Marzo'
        case 'April':
            return 'Abril'
        case 'May':
            return 'Mayo'
        case 'June':
            return 'Junio'
        case 'July':
            return 'Julio'
        case 'August':
            return 'Agosto'
        case 'September':
            return 'Septiembre'
        case 'Octuber':
            return 'Octubre'
        case 'November':
            return 'Noviembre'
        case 'December':
            return 'Diciembre'
      
         
      }
  }


function getDateWrittenEng(date) {
    date = moment(date);
    let day = date.format('D');
    let month = date.format('MMMM');
    let month1 = date.format('MM');
    let years = date.format('YYYY');
    return  convertNumberToWords(day,false)+' ('+day+') '+month+' ('+month1+') '+convertNumberToWords(years,false)+' ('+years+')'
    

}

function getDateWrittenSpa(date) {

    let day = date.format('D');
    let month = date.format('MMMM');
    let month1 = date.format('MM');
    let years = date.format('YYYY');
    return {
        dia: convertNumberToWordsSpa(day,false),
        diaNumber: day,
        mes: mountSpa(month),
        mesNumber: month1,
        years: convertNumberToWordsSpa(years,false),
        yearsNumber: years
    }    

}

function getNumberWritten(numbers) {
    return convertNumberToWords(numbers)
}








module.exports = {
    profesionRandom,
    email,
    celularRandom,
    telefonoRandom,
    calcularEdad,
    calculateNoClient,
    restDays,
    hotelRandom,
    getDaysByTwoDate,
    getAirline,
    qr,
    templateHotelRandom,
    hospitalRandom,
    vendedorRandom,
    contactoRandom,
    doctorRandom,
    templateSeguroRandom,
    doctorNameRandom,
    beforeHour,
    bookingRandom,
    dayForWeek,
    makeid,
    today,
    creditCard,
    getPriceByNight,
    addressPersonalRandom,
    familyName,
    removeAndAddDayDate,
    removeAndAddDayToday,
    hotelAddressDocuments,
    hotelNameDocuments,
    personaRandomName,
    positionRandom,
    departamentRandom,
    bancoRandom,
    getDateWrittenEng,
    getDateWrittenSpa,
    getNumberWritten,
    removeAndAddDayTodayForWritten,
    companies,
    parentRandomName,
    createTourTemplate
}
