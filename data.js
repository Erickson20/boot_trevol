const Helpers = require("./helpers.js")
const moment =  require("moment");



function data(passager){
        let hotels = Helpers.hotelRandom(passager.destino)
        console.log(`passager.nacimiento`, passager.nacimiento)
        
    return {
        "pasajeros":
            {
                "pais_destino": passager.destino,
                "nombre": passager.nombres,
                "apellidos": passager.apellidos,
                "fecha_entrada": moment(moment(passager.entrada,'DD-MM-YYYY')).format('DD-MM-YYYY'),
                "fecha_salida":moment(moment(passager.salida,'DD-MM-YYYY')).format('DD-MM-YYYY'),
                "aerolinea": Helpers.getAirline(passager.aerolinea),
                "numero_vuelo": passager.novuelo,
                "asiento": Helpers.calculateNoClient(10,28).toString(),
                "pais": "Dominican Republic-DO",
                "sexo": passager.sexo,
                "passaporte": passager.pasaporte,
                "nacionaldiad": "Dominican Republic-DO",
                "profesion": Helpers.profesionRandom(),
                "email": Helpers.email(passager.nombres,passager.apellidos),
                "codigo_cel": "República Dominicana (+1)",
                "celular": Helpers.celularRandom(),
                "codigo_tel": "República Dominicana (+1)",
                "vendedor": Helpers.vendedorRandom(),
                "contacto": Helpers.contactoRandom(),
                "doctor": Helpers.doctorRandom(),
                "telefono": Helpers.telefonoRandom(),
                "hotel_name": hotels.hotelName,
                "resultado":passager.resultado,
                "hora":moment(moment(passager.hora,'HH:MM')).format('HH:MM'),
                "hotel_direccion": hotels.direccion,
                "motivo_viaje":"Turismo",
                "via":"Aereo",
                "cedula":passager.cedula,
                "fecha_nacimiento": moment(moment(passager.nacimiento,'DD-MM-YYYY')).format('DD-MM-YYYY'),
            }

    }
}








module.exports = data;