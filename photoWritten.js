const tesseract = require("node-tesseract-ocr")

const config = {
  lang: "spa",
  oem: 1,
  psm: 3,
}

tesseract
  .recognize("josue.jpeg", config)
  .then((text) => {
    console.log("Result:", text)
  })
  .catch((error) => {
    console.log(error.message)
  })