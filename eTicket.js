const puppeteer = require('puppeteer-extra');
const RecaptchaPlugin = require('puppeteer-extra-plugin-recaptcha');
const moment = require("moment");
const Helpers = require("./helpers.js")



puppeteer.use(
    RecaptchaPlugin({
      provider: { id: '2captcha', token: '125d5d013ba9764502a095654a520f9d' }
    })
  );




async function main(data) {
    //'C:/Program Files/Google/Chrome/Application/chrome.exe
    const result =   data.pasajeros;
    const browser = await puppeteer.launch({
        headless: false,
        args: ['--no-sandbox', '--disable-setuid-sandbox', ]
    });

    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    await page.setViewport({ width: 1366, height: 768});
    await  page.goto("https://eticket.migracion.gob.do/Auth/TravelRegister/", {waitUntil: 'networkidle0'});
   //return

    // await page.evaluate(() => {
    //     document.querySelector('#request_ticket_btn').click();
    // });
    // await page.waitFor(1000);
    // await page.evaluate(() => {
    //     let test = document.querySelector('.swal2-confirm ') 
    //     test.click();
    // })
    // await page.waitFor(2000);
    console.log("RESOLVIENDO RECAPCHA ESPERE UN MOMENTO....");
    const { solved, error } = await page.solveRecaptchas();
    if(solved) {
        console.log('✔️ The captcha has been solved');
        await page.evaluate(() => {
            document.getElementById('btnSumbit').click();
        });
    }
    console.log("BIEN YA ESTAMOS HACIENDO EL ETICKET DE ->"+result.nombre+' '+result.apellidos);

    await page.waitForNavigation(true);
    await page.type("#permanentAddress", result.hotel_direccion)
    await page.click(".filter-option-inner-inner")
    await page.keyboard.type('Dominican Republic');
    await page.keyboard.press('Enter')
    await page.click(".modal-trigger")
    await page.waitFor(1000);
    await page.waitForSelector("#cityName");
    await page.type("#cityName","Provincia de Santo Domingo")
    await page.keyboard.press('Tab')
    await page.keyboard.press('Enter')
    await page.waitFor(1000);
    await page.waitForSelector(".list-group-item");
    await page.click(".list-group-item")
    await page.type("#ZipCode",Helpers.calculateNoClient(10403,13435).toString())
    await page.click(".toggle")
    await page.evaluate(() => {
        document.getElementById('StopOverInCountries').checked = true
    });

    
    // page.click('[id="IsArrival"] [value="False"]')
    await page.evaluate(() => {
        let test = document.querySelector('label[class="form-check-label exit"] > input')
        console.log('test :>> ', test);
        test.click()
    //    document.getElementById('IsArrival').checked = false
    //    document.querySelector('#IsArrival').click();
    });
    await Promise.all([
        page.evaluate(() => {
            let test = document.querySelector('.form-row >  div > button')
            test.click();
        }),
        page.waitForNavigation(true),
    ]);
    await page.waitForSelector("#MigratoryInformation_Names");
    await page.type("#MigratoryInformation_Names",result.nombre)
    await page.type("#MigratoryInformation_LastNames",result.apellidos)
    //await page.type("#dobSelect-1",result.fecha_nacimiento)
    console.log("eso>>>>>",moment(result.fecha_nacimiento, 'DD/MM/YYYY').format('M'));
    let month = parseInt(moment(result.fecha_nacimiento, 'DD/MM/YYYY').format('M'))-1
    await page.select("#dob-year-input-1",moment(result.fecha_nacimiento, 'DD/MM/YYYY').format('YYYY'))
    await page.select("#dob-month-input-1",month.toString())
    await page.select("#dob-day-input-1",moment(result.fecha_nacimiento, 'DD/MM/YYYY').format('DD'))

    await page.select('#MigratoryInformation_Gender',result.sexo)
    await page.select('select[name="MigratoryInformation.BirthPlace"]',"DOM");
    await page.select('#selectNat-1',"DOM");
    await page.type("#passportNum",result.passaporte.toString())
    await page.type("#passportConfirmation-1",result.passaporte.toString())
    await page.select('#MigratoryInformation_MaritalStatusId',"5");
    await page.select('select[name="MigratoryInformation.OcupationId"]',"5");
    await page.select('#MigratoryInformation_OcupationId',"5");
    //await page.select('#selectProvinces-1',"32");
    //await page.type("#txtStreet-1",result.hotel_direccion)
    await page.select('#MigratoryInformation_EmbarkationPortNavId',"27");
    //await page.type("#MigratoryInformation_EmbarcationFlightNumber",Helpers.calculateNoClient(2341,4352).toString())
    //await page.type("#MigratoryInformation_EmbarcationDate",moment(result.fecha_entrada, 'DD/MM/YYYY').format('DD/MM'))
    
    let month_f = parseInt(moment(result.fecha_entrada, 'DD/MM/YYYY').format('M'))-1
    await page.select("#flight-year-input-1",moment(result.fecha_entrada, 'DD/MM/YYYY').format('YYYY'))
    await page.select("#flight-month-input-1",month_f.toString())
    await page.select("#flight-day-input-1",moment(result.fecha_entrada, 'DD/MM/YYYY').format('DD'))

    await page.select('#MigratoryInformation_DisembarkationPortNavId',"50");
    await page.select('#MigratoryInformation_AirlineId',"847");
    await page.waitFor(2000);
   
   // let air = result.aerolinea === 'copa'? "755": result.aerolinea === 'avianca'? "480": "174";
    await page.type('#flight-number-input-1',"DM"+Helpers.calculateNoClient(2341,4352).toString()); //flight-number-input-1
    await page.type('#MigratoryInformation_ConfirmationNumber','VD'+Helpers.calculateNoClient(2341,4352).toString());
    //await page.select('#MigratoryInformation_DisembarkationPortNavId',"25");
    await page.select('#flightMotiveId-1',"6");


    await page.type("#MigratoryInformation_DaysOfStaying", "1")
    //await page.type("#MigratoryInformation_Email",result.email)

    await page.evaluate(() => {
        let test = document.querySelector('.form-group > button') 
        test.click();
    })
    await page.waitFor(1000)
    await page.evaluate(() => {
        let test = document.querySelector('.swal2-actions > .swal2-cancel ') 
        test.click();
    })
    await page.waitFor(2000)
    //await page.screenshot({path: 'pasajeros/'+result.nombre+' '+result.apellidos+'/eticket.png'});
    let path = 'pasajeros/'+result.nombre+' '+result.apellidos;
    try {
        await page.pdf({
            path: path+'/'+result.nombre+' '+result.apellidos+' - Salida ticket.pdf',
            displayHeaderFooter: false,
            printBackground: false,
            pageRanges: '1'});
    }catch (e) {
        console.log("error>: ",e)
    }
    console.log("HE TERMINADO CON EL ETICKET...");
    
    // await page.waitFor(2000)
    // await page.click("#Next-1")
    // await page.evaluate(() => {
    //     let test = document.querySelector('.swal2-actions > .swal2-cancel')
    //     test.click();
    // })
    // await page.waitFor(1000);
    // await page.evaluate(() => {
    //     let test = document.querySelector('.swal2-actions > .swal2-confirm')
    //     test.click();
    // })
    // await page.waitFor(1000);
    // await page.evaluate(() => {
    //     let test = document.querySelector('.swal2-actions > .swal2-cancel')
    //     test.click();
    // })

//     await page.waitForSelector(".form-group");

//     await Promise.all([
//         page.evaluate(() => {
//             let test = document.querySelector('.form-group > button')
//             test.click();
//         }),
//         page.waitForNavigation(true),
//     ]);
//     await page.evaluate(() => {
//         let test = document.querySelector('.swal2-actions > .swal2-cancel')
//         test.click();
//     })
//     await page.waitFor(2000);
//     console.log(`page.url()`, page.url())

//         page.evaluate( async ()  => {
//             let test = await document.querySelector('.col-md-4 > label > input')
//             test.click();
//         }),
//         page.waitForNavigation(true),


//     await page.type("#PhoneNumber",result.telefono)

//     await page.evaluate(() => {
//         let test = document.querySelector('.form-group > button')
//         test.click();
//     })
//     await page.waitFor(2000);
// //https://eticket.migracion.gob.do/TravelTicket/TicketEmision?token=7CACC2B4A18E10E9FDAF4ADF6321FA77
// console.log(`page.url()`, page.url())
//     await page.evaluate(() => {
//         let test = document.querySelector('.swal2-actions > .swal2-cancel')
//         test.click();
//     })
//     await page.evaluate(() => {
//         let test = document.querySelector('.swal2-actions > .swal2-confirm')
//         test.click();
//     })

    // await page.waitFor(5000);
    // let path = 'pasajeros/'+result.nombre+' '+result.apellidos;
    // try {
    //     const pdf = await page.pdf({
    //         path: path+'/'+result.nombre+' '+result.apellidos+' - ticket.pdf',
    //         displayHeaderFooter: false,
    //         printBackground: false,
    //         pageRanges: '1'});
    // }catch (e) {
    //     console.log("error>: ",e)
    // }
    // browser.close()

}

async function mainBack(data) {
    const result =   data.pasajeros;
    const browser = await puppeteer.launch({
        //headless: false,
        args: ['--no-sandbox', '--disable-setuid-sandbox', ]
    });

    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    await page.setViewport({ width: 1366, height: 768});
    await  page.goto("https://eticket.migracion.gob.do/Auth/TravelRegister/", {waitUntil: 'networkidle0'});
   

    // await page.evaluate(() => {
    //     document.querySelector('#request_ticket_btn').click();
    // });
    // await page.waitFor(1000);
    // await page.evaluate(() => {
    //     let test = document.querySelector('.swal2-confirm ') 
    //     test.click();
    // })
    // await page.waitFor(2000);

    console.log("RESOLVIENDO RECAPCHA ESPERE UN MOMENTO....");
    const { solved, error } = await page.solveRecaptchas();
    if(solved) {
        console.log('✔️ The captcha has been solved');
        await page.evaluate(() => {
            document.getElementById('btnSumbit').click();
        });
    }
    console.log("BIEN YA ESTAMOS HACIENDO EL ETICKET DE ->"+result.nombre+' '+result.apellidos);
    
    await page.waitForNavigation(true);
    await page.type("#permanentAddress", result.hotel_direccion)
    await page.click(".filter-option-inner-inner")
    await page.keyboard.type('Dominican Republic');
    await page.keyboard.press('Enter')
    await page.click(".modal-trigger")
    await page.waitFor(1000);
    await page.waitForSelector("#cityName");
    await page.type("#cityName","Provincia de Santo Domingo")
    await page.keyboard.press('Tab')
    await page.keyboard.press('Enter')
    await page.waitFor(1000);
    await page.waitForSelector(".list-group-item");
    await page.click(".list-group-item")
    await page.type("#ZipCode",Helpers.calculateNoClient(10403,13435).toString())
    await page.click(".toggle")
    await page.evaluate(() => {
        document.getElementById('StopOverInCountries').checked = true
    });

    
    // page.click('[id="IsArrival"] [value="False"]')
    await page.evaluate(() => {
        let test = document.querySelector('label[class="form-check-label entry"] > input')
        console.log('test :>> ', test);
        test.click()
    //    document.getElementById('IsArrival').checked = false
    //    document.querySelector('#IsArrival').click();
    });
    
    await Promise.all([
        page.evaluate(() => {
            let test = document.querySelector('.form-row >  div > button')
            test.click();
        }),
        page.waitForNavigation(true),
    ]);
    
    await page.waitForSelector("#MigratoryInformation_Names");
    await page.type("#MigratoryInformation_Names",result.nombre)
    await page.type("#MigratoryInformation_LastNames",result.apellidos)
    //await page.type("#dobSelect-1",result.fecha_nacimiento)
    console.log("eso>>>>>",moment(result.fecha_nacimiento, 'DD/MM/YYYY').format('M'));
    let month = parseInt(moment(result.fecha_nacimiento, 'DD/MM/YYYY').format('M'))-1
    await page.select("#dob-year-input-1",moment(result.fecha_nacimiento, 'DD/MM/YYYY').format('YYYY'))
    await page.select("#dob-month-input-1",month.toString())
    await page.select("#dob-day-input-1",moment(result.fecha_nacimiento, 'DD/MM/YYYY').format('DD'))

    await page.select('#MigratoryInformation_Gender',result.sexo)
    await page.select('select[name="MigratoryInformation.BirthPlace"]',"DOM");
    await page.select('#selectNat-1',"DOM");
    await page.type("#passportNum",result.passaporte)
    await page.type("#passportConfirmation-1",result.passaporte)
    await page.select('#MigratoryInformation_MaritalStatusId',"5");
    await page.select('select[name="MigratoryInformation.OcupationId"]',"5");
    await page.select('#MigratoryInformation_OcupationId',"5");
    
    //await page.select('#selectProvinces-1',"32");
    //await page.type("#txtStreet-1",result.hotel_direccion)
    await page.select('#MigratoryInformation_EmbarkationPortNavId',"27");
    await page.type("#MigratoryInformation_EmbarcationFlightNumber",Helpers.calculateNoClient(2341,4352).toString())
    //await page.type("#MigratoryInformation_EmbarcationDate",moment(result.fecha_entrada, 'DD/MM/YYYY').format('DD/MM'))
    let month_f = parseInt(moment(result.fecha_entrada, 'DD/MM/YYYY').format('M'))-1 

    await page.select("#flight-year-input-origin-1",moment(result.fecha_entrada, 'DD/MM/YYYY').format('YYYY'))
    await page.select("#flight-month-input-origin-1",month_f.toString())
    await page.select("#flight-day-input-origin-1",moment(result.fecha_entrada, 'DD/MM/YYYY').format('DD'))

    await page.select("#flight-year-input-1",moment(result.fecha_entrada, 'DD/MM/YYYY').format('YYYY'))
    await page.select("#flight-month-input-1",month_f.toString())
    await page.select("#flight-day-input-1",moment(result.fecha_entrada, 'DD/MM/YYYY').format('DD'))

    await page.select('#MigratoryInformation_DisembarkationPortNavId',"27");
    await page.type('#MigratoryInformation_OriginFlightNumber',Helpers.calculateNoClient(2341,4352).toString());
    await page.select("#MigratoryInformation_EmbarkationPortNavId","4423")
    await page.select('#MigratoryInformation_AirlineId',"847");
    await page.waitFor(2000);
    
   // let air = result.aerolinea === 'copa'? "755": result.aerolinea === 'avianca'? "480": "174";
    //await page.select('#MigratoryInformation_AirlineId',"28");
    await page.type('#MigratoryInformation_ConfirmationNumber','VD'+Helpers.calculateNoClient(2341,4352).toString());
    //await page.select('#MigratoryInformation_DisembarkationPortNavId',"25");
   // await page.select('#flightMotiveId-1',"6");

    
    //await page.type("#MigratoryInformation_DaysOfStaying", "1")
    //await page.type("#MigratoryInformation_Email",result.email)
    //#Next-1
    await page.evaluate(() => {
        let test = document.querySelector('.form-group > button') 
        test.click();
    })
    await page.waitFor(2000)
    await page.waitForSelector(".form-group > button");
    await page.evaluate(() => {
        let test = document.querySelector('#Next-1') 
        console.log('test :>> ', test);
        test.click();
    })
    await page.waitFor(1000)
     await page.evaluate(() => {
        let test = document.querySelector('.swal2-actions > .swal2-cancel')
        test.click();
    })
    await page.waitFor(1000)
    await page.evaluate(() => {
        let test = document.querySelector('.swal2-actions > .swal2-confirm')
         test.click();
    })
    await page.waitFor(1000)
    await page.evaluate(() => {
        let test = document.querySelector('.swal2-actions > .swal2-cancel')
         test.click();
    })
    await page.waitFor(1000)
    //await page.screenshot({path: 'pasajeros/'+result.nombre+' '+result.apellidos+'/eticket.png'});
    let path = 'pasajeros/'+result.nombre+' '+result.apellidos;
    try {
        await page.pdf({
            path: path+'/'+result.nombre+' '+result.apellidos+' - Entrada ticket.pdf',
            displayHeaderFooter: false,
            printBackground: false,
            pageRanges: '1'});
    }catch (e) {
        console.log("error>: ",e)
    }
    console.log("HE TERMINADO CON EL ETICKET...");
    
    // await page.waitFor(2000)
    // await page.click("#Next-1")
    // await page.evaluate(() => {
    //     let test = document.querySelector('.swal2-actions > .swal2-cancel')
    //     test.click();
    // })
    // await page.waitFor(1000);
    // await page.evaluate(() => {
    //     let test = document.querySelector('.swal2-actions > .swal2-confirm')
    //     test.click();
    // })
    // await page.waitFor(1000);
    // await page.evaluate(() => {
    //     let test = document.querySelector('.swal2-actions > .swal2-cancel')
    //     test.click();
    // })

//     await page.waitForSelector(".form-group");

//     await Promise.all([
//         page.evaluate(() => {
//             let test = document.querySelector('.form-group > button')
//             test.click();
//         }),
//         page.waitForNavigation(true),
//     ]);
//     await page.evaluate(() => {
//         let test = document.querySelector('.swal2-actions > .swal2-cancel')
//         test.click();
//     })
//     await page.waitFor(2000);
//     console.log(`page.url()`, page.url())

//         page.evaluate( async ()  => {
//             let test = await document.querySelector('.col-md-4 > label > input')
//             test.click();
//         }),
//         page.waitForNavigation(true),


//     await page.type("#PhoneNumber",result.telefono)

//     await page.evaluate(() => {
//         let test = document.querySelector('.form-group > button')
//         test.click();
//     })
//     await page.waitFor(2000);
// //https://eticket.migracion.gob.do/TravelTicket/TicketEmision?token=7CACC2B4A18E10E9FDAF4ADF6321FA77
// console.log(`page.url()`, page.url())
//     await page.evaluate(() => {
//         let test = document.querySelector('.swal2-actions > .swal2-cancel')
//         test.click();
//     })
//     await page.evaluate(() => {
//         let test = document.querySelector('.swal2-actions > .swal2-confirm')
//         test.click();
//     })

    // await page.waitFor(5000);
    // let path = 'pasajeros/'+result.nombre+' '+result.apellidos;
    // try {
    //     const pdf = await page.pdf({
    //         path: path+'/'+result.nombre+' '+result.apellidos+' - ticket.pdf',
    //         displayHeaderFooter: false,
    //         printBackground: false,
    //         pageRanges: '1'});
    // }catch (e) {
    //     console.log("error>: ",e)
    // }
    // browser.close()

}



module.exports = {
    main,
    mainBack
}
