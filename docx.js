const PizZip = require('pizzip');
const Docxtemplater = require('docxtemplater');
const fs = require('fs');
const path = require('path') ;
const Helpers = require("./helpers.js");
const moment = require("moment");
const Convert = require("./convertWordToPdf.js")


// The error object contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
function replaceErrors(key, value) {
    if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function(error, key) {
            error[key] = value[key];
            return error;
        }, {});
    }
    return value;
}

function errorHandler(error) {
    console.log(JSON.stringify({error: error}, replaceErrors));

    if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors.map(function (error) {
            return error.properties.explanation;
        }).join("\n");
        console.log('errorMessages', errorMessages);
        // errorMessages is a humanly readable message looking like this :
        // 'The tag beginning with "foobar" is unopened'
    }
    throw error;
}

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}



function pcr(result,template) {
    let dir = __dirname + '/pasajeros/'+result.nombre+' '+result.apellidos;
    let id_pcr = Helpers.calculateNoClient(70585151690,70796264794)
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o775);
    }

    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }


//set the templateVariables
    doc.setData({
        first_name: result.nombre,
        last_name: result.apellidos,
        cedula: result.cedula,
        telefono: result.telefono,
        fecha_nacimiento:result.fecha_nacimiento,
        sexo:result.sexo,
        qr_image:'./qrexample.png',
        no_pcr: id_pcr,
        doctor: Helpers.doctorRandom(),
        doctor_name:Helpers.doctorNameRandom(),
        no_pcr_salud:Helpers.calculateNoClient(15,38) +'-'+Helpers.calculateNoClient(23453,68952),
        hospital:Helpers.hospitalRandom(),
        before_hour: Helpers.beforeHour(result.hora),
        hour:result.hora,
        edad: "0"+Helpers.calcularEdad(result.fecha_nacimiento),
        edad_salud: Helpers.calcularEdad(result.fecha_nacimiento),
        no_client:"00"+Helpers.calculateNoClient(3252,4356),
        date_start: Helpers.restDays(result.fecha_entrada,3),
        date_colection: Helpers.restDays(result.fecha_entrada,2),
        date_report: Helpers.restDays(result.fecha_entrada,1),
        result:result.resultado? "DETECTADO/POSITIVO": "NO DETECTADO/NEGATIVO"

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({ type: "nodebuffer"});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir, result.nombre+ ' ' +result.apellidos+' - pcr.docx'), buf);
    //Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - pcr',dir)
    //Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - pcr.docx',dir, result.nombre+ ' ' +result.apellidos+' - pcr.pdf')
    

   
    let userInfor = {
        "name":result.nombre +' '+ result.apellidos,
        "id": result.cedula,
        "testDate":Helpers.restDays(result.fecha_entrada,1) + ' ' + Helpers.beforeHour(result.hora),
        "reportDate":Helpers.restDays(result.fecha_entrada,1) + ' ' + result.hora,
        "reportTime": result.hora,
        "result": result.resultado? "DETECTADO/POSITIVO": "NO DETECTADO/NEGATIVO"
    }

    let jsonString = JSON.stringify(userInfor);

    let buff = new Buffer(jsonString);
    let encoded = buff.toString('base64');
    buf.days
    console.log('jsonString :>> ', jsonString,result.resultado);
    console.log('encoded :>> ', encoded);
    let link = 'https://resultados.labrefrencia.com/autenticacion?id='+encoded
    Helpers.qr(dir,link)

}



function hotel(result,template) {
    let dir = __dirname + '/pasajeros/'+result.nombre+' '+result.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o775);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        first_name: result.nombre,
        last_name: result.apellidos,
        booking_id: Helpers.calculateNoClient(156111302,177232413) ,
        booking_ref: Helpers.calculateNoClient(3344738191,3455849294)  ,
        member_id: Helpers.calculateNoClient(222904,293456),
        hotel_name: result.hotel_name,
        hotel_address: result.hotel_direccion,
        hotel_country:capitalize(result.pais_destino),
        date_start: result.fecha_entrada,
        date_end: result.fecha_salida

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir, result.nombre+ ' ' +result.apellidos+' - hotel.docx'), buf);
    //Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - hotel',dir)
}



function recibo(result,template) {
    let dir = __dirname + '/pasajeros/'+result.nombre+' '+result.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o775);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    let nightResult = Helpers.getPriceByNight(parseInt(Helpers.getDaysByTwoDate(result.fecha_entrada,result.fecha_salida))-1)
    doc.setData({
        first_name: result.nombre,
        last_name: result.apellidos,
        booking_id: Helpers.calculateNoClient(156111302,177232413) ,
        booking_ref: Helpers.calculateNoClient(3344738191,3455849294)  ,
        member_id: Helpers.calculateNoClient(222904,293456),
        hotel_name: result.hotel_name,
        today: Helpers.today(),
        crediCard: Helpers.creditCard(),
        night: parseInt(Helpers.getDaysByTwoDate(result.fecha_entrada,result.fecha_salida))-1,
        nightPrice: nightResult.nightPrice,
        tax: nightResult.tax.toFixed(2),
        totalNight: nightResult.total.toFixed(2),
        hotel_address: result.hotel_direccion,
        hotel_country:capitalize(result.pais_destino),
        date_start: result.fecha_entrada,
        date_end: result.fecha_salida

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir, result.nombre+ ' ' +result.apellidos+' - recibo.docx'), buf);
    //Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - hotel',dir)
}

async function eticket(result,template) {
    let dir = __dirname + '/pasajeros/'+result.nombre+' '+result.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o775);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables
    doc.setData({
        first_name: result.nombre,
        last_name: result.apellidos,
        code: Helpers.makeid(5).toUpperCase() ,
        passport: result.passaporte  ,
        number: Helpers.calculateNoClient(1234,9803),
        date_start: result.fecha_entrada,

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx'), buf);
   // Helpers.convertToPdf(result.nombre+ ' ' +result.apellidos+' - eticket',dir)
   // await Convert.convetToPdf(dir, result.nombre+ ' ' +result.apellidos+' - eticket.docx',dir, result.nombre+ ' ' +result.apellidos+' - eticket.pdf')
}






function flightReturn(result,template) {
    let dir = __dirname + '/pasajeros/'+result.nombre+' '+result.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o775);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables

    doc.setData({
        first_name: result.nombre,
        last_name: result.apellidos,
        destiny: result.pais_destino.toUpperCase() == "SALVADOR"? "Salvador City (SAL)": "Guatemala City (GUA)",
        code_co: result.pais_destino.toUpperCase() == "SALVADOR"? "SAL": "GUA",
        country: result.pais_destino.toUpperCase() + " CITY",
        booking_number: Helpers.bookingRandom() ,
        number_flight: Helpers.calculateNoClient(1347826870308,1349996870308),
        date: Helpers.dayForWeek(moment(result.fecha_salida,'DD-MM-YYYY').day()).toUpperCase() + ', '+ moment(result.fecha_salida,'DD-MM-YYYY').format("MMM DD").toUpperCase(),
        date_next: Helpers.dayForWeek(moment(result.fecha_salida,'DD-MM-YYYY').add(1, 'days').day()).toUpperCase() + ', '+ moment(result.fecha_salida,'DD-MM-YYYY').add(1, 'days').format("MMM DD").toUpperCase(),

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir, result.nombre+ ' ' +result.apellidos+' - regreso.docx'), buf);
    //fs.writeFileSync(path.resolve(dir, result.nombre+ ' ' +result.apellidos+' - regreso.pdf'), buf);
}



function seguro(result,template) {
    let dir = __dirname + '/pasajeros/'+result.nombre+' '+result.apellidos;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o775);
    }
    var content = fs
        .readFileSync(path.resolve(__dirname, template), 'binary');

    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater(zip);
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }

//set the templateVariables

    let importe  = (Helpers.getDaysByTwoDate(result.fecha_entrada,result.fecha_salida) * 10) * 58.00
    doc.setData({
        first_name: result.nombre.toUpperCase(),
        last_name: result.apellidos.toUpperCase(),
        fecha_nacimiento:result.fecha_nacimiento,
        passaporte: result.passaporte,
        voucher: Helpers.calculateNoClient(9554528,9932438)  ,
        control: Helpers.calculateNoClient(333628,393628),
        phone2:Helpers.calculateNoClient(8092338026,8098338026),
        today:Helpers.restDays(result.fecha_entrada,1),
        pais:result.pais_destino,
        email:result.email,
        days: Helpers.getDaysByTwoDate(result.fecha_entrada,result.fecha_salida),
        contacto: Helpers.contactoRandom().toUpperCase(),
        vendedor: Helpers.vendedorRandom().toUpperCase(),
        phone: result.telefono,
        passaporte:result.passaporte,
        importe: new Intl.NumberFormat().format(importe),
        emitido:Helpers.restDays(result.fecha_entrada, '1'),
        date_start: result.fecha_entrada,
        date_end: result.fecha_salida

    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
        errorHandler(error);
    }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(dir, result.nombre+ ' ' +result.apellidos+' - seguro.docx'), buf);
}



module.exports = {
    pcr,
    hotel,
    seguro,
    flightReturn,
    eticket,
    recibo
}

